for i in 1 2 3 4 5 6 7 8 ; do
 
   read a
   clear
   if [ $i -ge 1 ]; then
     echo You are given a vector of integers between 0 and '10^9', whose size is between 1 and 100,000
   fi
   if [ $i -ge 2 ]; then
     echo ""
     echo "You are also given an integer p between 0 and half the vector's length."
   fi
   if [ $i -ge 3 ]; then
     echo ""
     echo Your goal is to find p distinct pairs of numbers in the vector, 
     echo whose maximum difference is minimized.
   fi
   if [ $i -ge 4 ]; then
     echo ""
     echo The vector may contain duplicates -- 
     echo If there are d duplicate values of the number x, 
     echo then you can use x up to d times in your pairs.
   fi
   if [ $i -ge 5 ]; then
     echo ""
     echo Return the minimum maximum difference.
   fi

   if [ $i -ge 6 ]; then
     echo ""
     echo "Example 1: nums = [10, 1, 2, 7, 1, 3 ], p = 1.    Answer = 0."
   fi

   if [ $i -ge 7 ]; then
     echo ""
     echo "Example 2: nums = [10, 1, 2, 7, 1, 3 ], p = 2.    Answer = 1."
   fi

   if [ $i -ge 8 ]; then
     echo ""
     echo "Example 3: nums = [10, 1, 2, 7, 1, 3 ], p = 3.    Answer = 3."
   fi

done

read a
