for i in 1 2 3 4 5 6 7 8 ; do
 
   read a
   clear
   if [ $i -ge 1 ]; then
     echo You are given a vector of integers called piles. 
     echo 'Its values are between 1 and 10^9, and its length is between 1 and 10,000.'
   fi
   if [ $i -ge 2 ]; then
     echo ""
     echo 'You are given a value h, which is between piles.size() and 10^9.'
   fi
   if [ $i -ge 3 ]; then
     echo ""
     echo You are to derive a value k, which works as follows.
   fi
   if [ $i -ge 4 ]; then
     echo ""
     echo At each timestep, you may reduce the value of a pile by k. 
     echo You may not reduce the value below zero, 
     echo and you may not reduce the value of more than one pile in a timestep.
   fi
   if [ $i -ge 5 ]; then
     echo ""
     echo What is the minimum value of k that allows you to reduce all of the piles
     echo to zero in at most h timesteps.
   fi
   if [ $i -ge 6 ]; then
     echo ""
     echo 'Example 1: piles = [ 3, 6, 7, 11 ], h=8.    Answer = 4.'
   fi

   if [ $i -ge 7 ]; then
     echo ""
     echo 'Example 2: piles = piles = [ 30, 11, 23, 4, 20 ], h=5.   Answer = 30.'
   fi

   if [ $i -ge 8 ]; then
     echo ""
     echo 'Example 3: piles = piles = [ 30, 11, 23, 4, 20 ], h=6.   Answer = 23.'
   fi
done

read a
