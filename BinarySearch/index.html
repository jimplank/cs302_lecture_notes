<title>CS302 Lecture Notes</title>
<h2>CS302 Lecture Notes - Binary Search</h2>
<UL>
<LI> August, 2023.  Latest revision: August, 2023.
<LI> James S. Plank
<LI> Directory: <b>/home/jplank/cs302/Notes/BinarySearch</b>
</UL>
<hr>

<h3>Reference Material Online / Topcoder / Leetcode Problem(s)</h3>

<UL>
<LI> <a href=https://en.wikipedia.org/wiki/Binary_search_algorithm>Binary Search from Wikipedia</a>
<LI> <a href=http://web.eecs.utk.edu/~jplank/topcoder-writeups/Leetcode/Single-Element-In-A-Sorted-Array/>Leetcode.com problem 540: Single Element in a Sorted Array.</a> Detailed writeup, but no solution given.
<LI> <a href=http://web.eecs.utk.edu/~jplank/topcoder-writeups/Leetcode/Successful-Pairs-Of-Spells/index.html>Leetcode.com problem 2300: Successful Pairs of Spells and Potions</a>.
Detailed writeup, but no solution given.
<LI> <a href=http://web.eecs.utk.edu/~jplank/topcoder-writeups/2017/RememberWords/index.html>Topcoder SRM 721, D1, 250-Pointer (RememberWords)</a>.
Brief writeup, but no solution given.
<LI> <a href=http://web.eecs.utk.edu/~jplank/topcoder-writeups/2017/SellingFruits/index.html>Topcoder SRM 706, D2, 500-Pointer (Selling Fruit)</a>.
Brief writeup, but no solution given.
<LI> <a href=http://web.eecs.utk.edu/~jplank/topcoder-writeups/Leetcode/Median-Of-Two-Sorted-Arrays/index.html>Leetcode.com problem 4: "Median-Of-Two-Sorted-Arrays"</a>.
Detailed writeup, but no solution given.  This is a tough one!
<LI> <a href=https://leetcode.com/problems/capacity-to-ship-packages-within-d-days/>Leetcode.com problem 1011. Capacity To Ship Packages Within D Days</a>.  No writeup or solution -- this one is similar to "Koko Eating Bananas" below, plus the Leetcode editorial is unlocked by default, so that can help if you need it.
<LI> <a href=https://leetcode.com/problems/search-a-2d-matrix>Leetcode.com problem 74. Search a 2D Matrix</a>.  Two binary searhes.  No writeup or solution.
<LI> <a href=https://leetcode.com/problems/maximum-value-at-a-given-index-in-a-bounded-array>Leetcode.com problem 1802. Maximum Value at a Given Index in a Bounded Array</a>.  No writeup, but the Leetcode editorial is unlocked by default, so that can help if you need it.
</UL>

<hr>
<h3>What do you need to know for the exam?</h3>

<UL>
<LI> How to perform a binary search.
<LI> Its running time.
<LI> How to identify problems as being solvable with binary search.
</ul>

<hr>
<h3>Overview</h3>

Binary search is a very powerful problem-solving technique.  The idea is simple.
Given a collection of <i>n</i> items and problem to solve, you solve the problem by 
throwing away half of the items, and then solving the problem on the half that you have
remaining.
<p>
I know that's a bit vague, but that's the general principle.  The running time is straightforward,
too: At each step of your problem, you're throwing away half of the items, so after <i>log(n)</i>
steps, you'll be left with one item, and you're done.  
<p>
That means your binary search is generally <i>O(log n)</i>, which is smoking fast.  I say
"generally", because sometimes you need to do extra stuff during a step, and that can increase
your overhead.

<hr>
<h3>Conceptually Easy, But Typically Harder</h3>

When you go to implement a binary search, you'll find that your code
is often buggy, and you need some inelegant edge conditions.  That's
normal, and you should be ready for it.  The reason that I decided to
teach binary search in CS302 (starting 2023) is that every time I had
to do a binary search problem in Leetcode, it took extra attention to
detail and extra testing.  I have some helpful advice, I believe, in
these lecture notes, but experience is always the best teacher.

<hr>
<h3>A Canoncial Example</h3>

Here's a canonical example.  The file
<b><a href=txt/words.txt>txt/words.txt</a></b> contains a dictionary of 24,853 words,
all lower-case, sorted:

<pre>
UNIX> <font color=darkred><b>wc txt/words.txt </b></font>
   24853   24853  205393 txt/words.txt           # There are 24,853 words
UNIX> <font color=darkred><b>head -n 5 txt/words.txt                   <font color=blue> # It looks sorted from the beginning</b></font></font>
aaa
aaas
aarhus
aaron
aau
UNIX> <font color=darkred><b>tail -n 5 txt/words.txt                   <font color=blue> # And end</b></font></font>
zoroastrian
zounds
zucchini
zurich
zygote
UNIX> <font color=darkred><b>sort txt/words.txt > tmp.txt              <font color=blue> # We demonstrate that it is indeed sorted.</b></font></font>
UNIX> <font color=darkred><b>diff txt/words.txt tmp.txt</b></font>
UNIX> <font color=darkred><b></b></font>
</pre>

Let's write a program, in 
<b><a href=src/dict_bsearch.cpp>src/dict_bsearch.cpp</a></b>.  It's a straightforward
binary search -- please read the comments inline for explanation.

<p><center><table border=3 cellpadding=3><td><pre>
<font color=blue>/* src/dict_bsearch.cpp.  This reads a dictionary of words from a file into a vector of strings.
   It sorts the vector, if not already sorted, and then it reads words from standard input
   It uses binary search to determine whether each of these words is in the dictionary. */</font>

#include &lt;vector&gt;
#include &lt;algorithm&gt;
#include &lt;iostream&gt;
#include &lt;fstream&gt;
using namespace std;

class Bsearch {
  public:
    void Create(const string &filename);        <font color=blue>// Create the vector from the file.</font>
    bool Find(const string &word) const;        <font color=blue>// Return whether a word is in the vector.</font>
  protected: 
    vector &lt;string&gt; words;   
};

<font color=blue>/* Create() is straightforward -- it reads each words into a vector, and while doing so,
   determines whether the vector is sorted.  If it is not, then it is sorted at the
   end of Create(). */</font>

void Bsearch::Create(const string &filename)
{
  ifstream fin;
  bool sorted;
  string w;

  sorted = true;

  fin.open(filename.c_str());
  if (fin.fail()) throw (string) "Could not open " + filename;
  while (fin &gt;&gt; w) {
    if (words.size() &gt; 0 && w &lt; words[words.size()-1]) sorted = false;
    words.push_back(w);
  }
  if (!sorted) sort(words.begin(), words.end());
}

<font color=blue>/* Here's the binary search.  It keeps track of three variables:

   l = the index of the smallest word that we are considering.
   h = the index of the largest word that we are considering.
   m = the middle of l and h

   It iterates by looking at words[m], and using that value to either return,
   discard the lower half of elements or discard the higher half of elements.
 */</font>

bool Bsearch::Find(const string &word) const
{
  int l, h, m;

  if (words.size() == 0) return false;

  l = 0;                    <font color=blue> // Initially, we consider the entire vector</font>
  h = words.size() - 1;

  while (l &lt;= h) {
    m = (l + h) / 2; 
    <font color=blue>// printf("l:%d(%s)  m:%d(%s)  h:%d(%s)\n", </font>
    <font color=blue>//          l, words[l].c_str(), m, words[m].c_str(), h, words[h].c_str());</font>
    if (words[m] == word) return true;
    if (words[m] &gt; word) h = m-1;    <font color=blue> // Throw away the top half</font>
    if (words[m] &lt; word) l = m+1;    <font color=blue> // Throw away the bottom half</font>
  }
  return false;

}

<font color=blue>/* The main() is straightfoward -- create the dictionary from the file, then find each
   word on standard input. */</font>

<font color=blue>/* I'm not including it here in the lecture notes. */</font>
</pre></td></table></center><p>


Let's test by uncommenting the <b>printf()</b> statement inside <b>Find()</b>, and doing some
small examples:

<pre>
UNIX> <font color=darkred><b>wc txt/words-12.txt</b></font>         <font color=blue> # My dictionary has 12 words</font>
      12      12      99 txt/words-12.txt
UNIX> <font color=darkred><b>cat txt/words-12.txt</b></font>         
attention
debtor
efficient
goldenseal
highwaymen
hogan
moth
rebutted
salsify
stud
wakeful
woodpeck

            <font color=blue> # I'm going to find attention, debtor, efficient and hogan, which are all there:</font>

UNIX> <font color=darkred><b>echo attention | bin/dict_bsearch txt/words-12.txt y</b></font>
l:0(attention)  m:5(hogan)  h:11(woodpeck)
l:0(attention)  m:2(efficient)  h:4(highwaymen)
l:0(attention)  m:0(attention)  h:1(debtor)
attention: found
Found: 1 of 1

UNIX> <font color=darkred><b>echo debtor | bin/dict_bsearch txt/words-12.txt y</b></font>
l:0(attention)  m:5(hogan)  h:11(woodpeck)
l:0(attention)  m:2(efficient)  h:4(highwaymen)
l:0(attention)  m:0(attention)  h:1(debtor)
l:1(debtor)  m:1(debtor)  h:1(debtor)
debtor: found
Found: 1 of 1

UNIX> <font color=darkred><b>echo efficient | bin/dict_bsearch txt/words-12.txt y</b></font>
l:0(attention)  m:5(hogan)  h:11(woodpeck)
l:0(attention)  m:2(efficient)  h:4(highwaymen)
efficient: found
Found: 1 of 1

UNIX> <font color=darkred><b>echo hogan | bin/dict_bsearch txt/words-12.txt y</b></font>
l:0(attention)  m:5(hogan)  h:11(woodpeck)
hogan: found
Found: 1 of 1

            <font color=blue> # Now aaa, zzz and mmm, which are not there:</font>

UNIX> <font color=darkred><b>echo aaa | bin/dict_bsearch txt/words-12.txt y</b></font>
l:0(attention)  m:5(hogan)  h:11(woodpeck)
l:0(attention)  m:2(efficient)  h:4(highwaymen)
l:0(attention)  m:0(attention)  h:1(debtor)
aaa: not-found
Found: 0 of 1

UNIX> <font color=darkred><b>echo zzz | bin/dict_bsearch txt/words-12.txt y</b></font>
l:0(attention)  m:5(hogan)  h:11(woodpeck)
l:6(moth)  m:8(salsify)  h:11(woodpeck)
l:9(stud)  m:10(wakeful)  h:11(woodpeck)
l:11(woodpeck)  m:11(woodpeck)  h:11(woodpeck)
zzz: not-found
Found: 0 of 1

UNIX> <font color=darkred><b>echo mmm | bin/dict_bsearch txt/words-12.txt y</b></font>
l:0(attention)  m:5(hogan)  h:11(woodpeck)
l:6(moth)  m:8(salsify)  h:11(woodpeck)
l:6(moth)  m:6(moth)  h:7(rebutted)
mmm: not-found
Found: 0 of 1

UNIX> <font color=darkred><b></b></font>
</pre>

It's a good idea to go over the examples above and look at the indices, to see how it hones
the search space at each step.  Let's look at a bigger example to see what happens when
it tries to find "jjj" in <b>txt/words.txt</b>.  I'm going to have that print statement
print <i>(h-l)</i> at each step, so you can see how it roughly halves at each step:

<pre>
UNIX> <font color=darkred><b>echo jjj | bin/dict_bsearch txt/words.txt y</b></font>
h-l:<font color=blue>24852</font> l:0(aaa)  m:12426(jewelry)  h:24852(zygote)
h-l:<font color=blue>12425</font> l:12427(jewett)  m:18639(refractory)  h:24852(zygote)
h-l:<font color=blue>6211</font> l:12427(jewett)  m:15532(nightfall)  h:18638(refractometer)
h-l:<font color=blue>3104</font> l:12427(jewett)  m:13979(mambo)  h:15531(nightdress)
h-l:<font color=blue>1551</font> l:12427(jewett)  m:13202(legendary)  h:13978(maltreat)
h-l:<font color=blue>774</font> l:12427(jewett)  m:12814(knapsack)  h:13201(legend)
h-l:<font color=blue>386</font> l:12427(jewett)  m:12620(kamchatka)  h:12813(knapp)
h-l:<font color=blue>192</font> l:12427(jewett)  m:12523(joyous)  h:12619(kalmuk)
h-l:<font color=blue>95</font> l:12427(jewett)  m:12474(johns)  h:12522(joyful)
h-l:<font color=blue>46</font> l:12427(jewett)  m:12450(joanna)  h:12473(johnny)
h-l:<font color=blue>22</font> l:12427(jewett)  m:12438(jimenez)  h:12449(joan)
h-l:<font color=blue>10</font> l:12439(jimmie)  m:12444(jitterbug)  h:12449(joan)
h-l:<font color=blue>4</font> l:12445(jitterbugger)  m:12447(jittery)  h:12449(joan)
h-l:<font color=blue>1</font> l:12448(jive)  m:12448(jive)  h:12449(joan)
h-l:<font color=blue>0</font> l:12449(joan)  m:12449(joan)  h:12449(joan)
jjj: not-found
Found: 0 of 1
UNIX> <font color=darkred><b></b></font>
</pre>

<hr>
<h3>How does binary search compare with sets and unordered sets?</h3>

If I asked you to write the program above without binary search, but instead using the
standard template library, I hope you would consider the following:

<UL>
<LI> A <b>set</b>.  Creating the set is <i>O(n log n)</i>, where <i>n</i> is the number of
     words in the dictionary, and then performing the <b>Finds</b> is <i>O(m log n)</i>,
     where <i>m</i> is the number of <b>Finds</b>.
<p>
<LI> An <b>unordered_set</b>.  This employs a hash table under the hood -- creating it is
     <i>O(n)</i> and performing the <b>Finds</b> is <i>O(m)</i>.  
</UL>

That argues for the <b>unordered_set</b>.  Where does binary search fit it?  Well, creating
the vector is <i>O(n)</i> if it's sorted, and <i>O(n log n)</i> if it's not.  And performing
the <b>Finds</b> is <i>O(m log n)</i>.  In other words, identical to the <b>set</b>.
<p>
I have implemented the <b>set</b> and <b>unordered_set</b> code in
<b><a href=src/dict_set.cpp>src/dict_set.cpp</a></b> and
<b><a href=src/dict_uset.cpp>src/dict_uset.cpp</a></b> respectively.  To test, 
I have created
<b><a href=txt/test.txt>txt/test.txt</a></b>, which has 12,000 words from 
<b><a href=txt/words.txt>txt/words.txt</a></b>, and 12,000 words that are not in 
<b><a href=txt/words.txt>txt/words.txt</a></b>.  Let's see how they compare:

<pre>
UNIX> <font color=darkred><b>make clean</b></font>
rm -f bin/*
UNIX> <font color=darkred><b>make bin/dict_bsearch bin/dict_set bin/dict_uset</b></font>
g++ -o bin/dict_bsearch -Wall -Wextra -std=c++11 src/dict_bsearch.cpp
g++ -o bin/dict_set -Wall -Wextra -std=c++11 src/dict_set.cpp
g++ -o bin/dict_uset -Wall -Wextra -std=c++11 src/dict_uset.cpp
UNIX> <font color=darkred><b>time <font color=blue>bin/dict_bsearch</font> txt/words.txt < txt/test.txt n</b></font>
Found: 12000 of 24000

<font color=blue>real	0m0.151s</font>
<font color=blue>user	0m0.147s</font>
sys	0m0.003s

UNIX> <font color=darkred><b>time <font color=blue>bin/dict_set</font> txt/words.txt < txt/test.txt n</b></font>
Found: 12000 of 24000

<font color=blue>real	0m0.226s</font>
<font color=blue>user	0m0.220s</font>
sys	0m0.004s

UNIX> <font color=darkred><b>time <font color=blue>bin/dict_uset</font> txt/words.txt < txt/test.txt n</b></font>
Found: 12000 of 24000
<font color=blue>real	0m0.089s</font>
<font color=blue>user	0m0.084s</font>
sys	0m0.003s

UNIX> <font color=darkred><b></b></font>
</pre>

Predictably, the <b>unordered_set</b> was the fastest.  The binary search is significantly faster
than the <b>set</b>, even though they have the same big-O.  Part of that is because we don't 
sort the words (they are already sorted), but the significant savings actually come from memory.
The <b>set</b> uses a tree data structure, which has a lot of pointers and extra memory.
The binary search simply uses the vector.
<p>
Keep that in mind.

<hr>
<h3>Using start/size rather than low/high</h3>

Rather than using low/high (or left-right) indices, you can specify the part of the vector
under review using:

<UL>
<LI> <b>Start:</b> This is the index of the first element of the vector that we are testing.
<LI> <b>Size:</b> This is the size of the region of the vector that we are testing.
</UL>

After spending quite a bit of time implementing binary searches using both types of information,
I have concluded that I like keeping track of <b>start</b> and <b>size</b> better than <b>low</b>
and <b>high</b>.  The main reason is that so long as you make sure that <b>size</b> is greater
than zero, then <b>words[start]</b> is always valid.  I find that comforting.  Here's the 
implementation of <b>Find()</b> using <b>start/size</b>.  The code is in
<b><a href=src/dict_bs_ss.cpp>src/dict_bs_ss.cpp</a></b>:

<p><center><table border=3 cellpadding=3><td><pre>
bool Bsearch::Find(const string &word) const
{
  int start, size, mid;

  start = 0;
  size = words.size();
  if (size == 0) return false;

  while (size &gt; 1) {
    mid = start + size/2;

    <font color=blue>/* I guess I like how this code translates logically: */</font>

    if (words[mid] &gt; word) {  <font color=blue>/* If word is not in the second half... */</font>
      size /= 2;              <font color=blue>/* Discard the second half. */</font>
    } else {
      start += size/2;        <font color=blue>/* Otherwise discard the first half. */</font>
      size -= size/2;         <font color=blue>/* Note this handles even and odd sizes correctly. */</font>
    }
  }
  return (words[start] == word);
}
</pre></td></table></center><p>

It's faster than the previous code (it was about 0.151 rather than 0.114 here):

<pre>
UNIX> <font color=darkred><b>time bin/dict_bs_ss txt/words.txt < txt/test.txt n</b></font>
Found: 12000 of 24000

<font color=blue>real	0m0.114s</font>.
<font color=blue>user	0m0.110s</font>.
sys	0m0.003s
UNIX>
</pre>

Plus it has another advantage of the previous code -- if the vector contains duplicates,
this will always return the last of the duplicates.

<hr>
<h3>Taking a look at that visually</h3>

I find this graphic really helpful when thinking about binary searches:

<p><center><table border=3><td><a href=jpg/start-size.jpg><img src=jpg/start-size.jpg width=500></a></td></table></center><p>

Your goal is to either eliminate the light blue region or the pink region.
Sometimes that's pretty natural, as in the code above.  Sometimes less so.
The next section is an example.

<hr>
<h3>Less obvious uses of binary search -- optimization</h3>

When you're trying to spot a binary search solution to a problem, see if you can frame
the problem in the following way:
<p>
Let me define a function <i>f(v)</i> whose answer is "yes/no".  Let's suppose that <i>f(v)</i>
is <i>O(n)</i>.  Moreover, suppose that there is a 
value <i>v<sub>opt</sub></i>, such athat for all <i>v < v<sub>opt</sub></i>, 
<i>f(v)</i> is "yes", and for all 
<i>v > v<sub>opt</sub></i>, 
<i>f(v)</i> is "no".  Then we can use binary search on <i>v</i> to find <i>v<sub>opt</sub></i>.
The running time is going to be <i>O(n log v)</i>.
<p>
A good example of this is Leetcode Medium problem #875: "Koko Eating Bananas".  Here's
the problem on Leetcode if you want to try it yourself:
<a href=https://leetcode.com/problems/koko-eating-bananas/>https://leetcode.com/problems/koko-eating-bananas/</a>.
<p>
Here's a summary of the problem:

<UL>
<LI> You are given a vector of integers called <b>piles</b>.  Its values are between
1 and 10<sup>9</sup>, and its length is between 1 and 10,000.
<LI> You are given a value <b>h</b>, which is between <b>piles.size()</b> and 10<sup>9</sup>.
<LI> You are to derive a value <b>k</b>, which works as follows.
<LI> At each timestep, you may reduce the value of a pile by <b>k</b>.  You may not reduce
     the value below zero, and you may not reduce the value of more than one pile in a timestep.
<LI> What is the minimum value of <b>k</b> that allows you to reduce all of the piles to zero
     in at most <b>h</b> timesteps.
</UL>

Let's work through the Leetcode examples:

<UL>
<LI> Example 1: <b>piles = [ 3, 6, 7, 11 ]</b>, <b>h=8</b>.
<p>
The answer is 4:
<UL>
<LI> 1 timestep for <b>piles[0]</b> = 3.
<LI> 2 timesteps for <b>piles[1]</b> = 6.
<LI> 2 timesteps for <b>piles[2]</b> = 7.
<LI> 3 timesteps for <b>piles[3]</b> = 11.
</UL>

That's 8 timesteps.  You'll note that if you set <b>k</b> to 3, then it will take you 10
timesteps.
<p>

<LI> Example 2: <b>piles = [ 30, 11, 23, 4, 20 ]</b>, <b>h=5</b>.
<p>
The answer is 30, because you have to reduce each pile to zero in a single timestep.
<p>

<LI> Example 3: <b>piles = [ 30, 11, 23, 4, 20 ]</b>, <b>h=6</b>.
<p>
The answer is 23.  That way, you get piles 1 through 4 in one timestep, and pile 0 in two
tiemsteps.
<p>
</UL>

This is a typical topcoder/leetcode problem, because your first inclination is to think in
terms how you break up the individual piles.  However, resist that temptation.  Instead,
focus on the observations which point toward binary search:

<OL>
<LI> If you set <i>k</i> to zero, you'll never succeed.
<LI> If you set <i>k</i> to the maximum pile size, you'll finish in exactly <b>piles.size()</b>
     timesteps.  In other words, youll always succeed.
<LI> If you have a value of <i>k</i> and you increase it, then the number of timesteps will stay
     the same or be reduced.
<LI> If you have a value of <i>k</i> and you decreases it, then the number of timesteps will stay
     the same or be increased.
</OL>

This gives you all of the conditions that you need for binary search.  Let <i>f(k)</i> = "yes" if
you can succeed with a value of k, and "no" if you cannot.  
You know that 
<i>k<sub>opt</sub></i>.
is going to be greater than zero and less than or equal to the maximum pile size.  So
use binary search to find <i>k<sub>opt</sub></i>.  

<p>
Here are the broad strokes:
<p>

<UL>
<LI> Test the middle element for <i>k</i>.  If you can succeed, discard the higher half.  If you
     can't, discard the lower half.  
<LI> An important thing is that you <i>know</i> that your answer is in the region that you're
     considering.  Thus, the binary search works.
</UL>

It's actually a little more complex than that.  Let's return to the picture above:

<p><center><table border=3><td><a href=jpg/start-size.jpg><img src=jpg/start-size.jpg width=500></a></td></table></center><p>

Suppose we set <b>mid</b> to <b>start+size/2</b> and then we test to see <b>mid</b>
is a successful value of <i>k</i>.  If it is, then we can throw out all of the values
greater than <b>mid</b>.  That's not what we want.  
Instead, set <b>mid</b> to <b>start+size/2-1</b> -- that's the last element in the blue
region above.  If it is successful, then we can throw out the pink region.  If it's not,
then we can throw out the blue region.  That's just what we want!

<p>
If you want to work on this yourself, you can start with the skeleton program in
<b><a href=src/Koko_Skeleton.cpp>src/Koko_Skeleton.cpp</a></b>.  This sets up a driver
program and a skeleton (incorrect) implementation of the Leetcode method "minEatingSpeed".
If we compile and run, it always returns zero:

<pre>
UNIX> <font color=darkred><b>g++ src/Koko_Skeleton.cpp </b></font>
UNIX> <font color=darkred><b>echo 3 6 7 11    8 | ./a.out</b></font>       <font color=blue> # Example 1</font>
0
UNIX> <font color=darkred><b>echo 30 11 23 4 20   5 | ./a.out</b></font>   <font color=blue> # Example 2</font>
0
UNIX> <font color=darkred><b></b></font>
</pre>

Below is the commented answer, using binary search.
It's in <b><a href=src/Koko_Solution.cpp>src/Koko_Solution.cpp</a></b>.  Here's the
minEatingSpeed method:

<p><center><table border=3 cellpadding=3><td><pre>
int Solution::minEatingSpeed(vector&lt;int&gt;& piles, int h)
{
  int start, size, maxpile, mid;
  int i;
  long long timesteps, for_pile;

  /* Calculate the maximum pile size. */

  maxpile = piles[0];
  for (i = 0; i &lt; piles.size(); i++) if (piles[i] &gt; maxpile) maxpile = piles[i];

  <font color=blue>/* We want our range to start at one and end at maxpile (including maxpile).
     So we set start to 1 and size to maxpile.  Remember size means that start+size is one
     past the last element in our region. */</font>

  start = 1;

  while (size &gt; 1) {

    <font color=blue>/* You want to test the highest value in the first half of the values (
       the last value in the blue region of the picture. */</font>

    mid = start + size/2 - 1;
    
    <font color=blue>/* Timesteps is the total timesteps if k is set to mid. */</font>

    timesteps = 0;
    for (i = 0; i &lt; piles.size(); i++) {
      for_pile = piles[i] / mid;
      if (piles[i]%mid != 0) for_pile++;
      timesteps += for_pile;
    } 

    // printf("Start: %d.  Size: %d  Mid: %d.  Timesteps: %lld\n", start, size, mid, timesteps);

    <font color=blue>/* If timesteps is too big, then you know that the answer
       is in the second half of the range.  You can throw out the
       first half. */</font>

    if (timesteps &gt; h) {
      start += size/2;
      size -= size/2;      

    <font color=blue>/* Otherwise, the answer is in the first half of the range, so toss out the second half. */</font>

    } else {
      size = size/2;
    }
  }
  return start;
}
</pre></td></table></center><p>

(If you care, that solution was pretty much smack at 50% in terms of speed on Leetcode).

<hr>
<h3>Another Optimization Problem</h3>

This is Leetcode problem 2616: "Minimize the Maximum Difference of Pairs."  Here's
the link: <a href=https://leetcode.com/problems/minimize-the-maximum-difference-of-pairs/>
https://leetcode.com/problems/minimize-the-maximum-difference-of-pairs/</a>.
<p>
Here's my description:

<UL>
<LI> You are given a vector of integers between 0 and 10<sup>9</sup>, whose size is between 
1 and 100,000.
<LI> You are also given an integer <i>p</i> between 0 and half the vector's length.
<LI> Your goal is to find <i>p</i> distinct pairs of numbers in the vector, whose maximum difference
is minimized.
<LI> The vector may contain duplicates -- if there are <i>d</i> duplicate values of the number
<i>x</i>, then you can use <i>x</i> up to <i>d</i> times in your pairs.
<LI> Return the minimum maximum difference. (ha ha).
</UL>

Examples help.  Let's use their example one:
<pre>
nums = [10, 1, 2, 7, 1, 3 ]
p = 2
</pre>
So we need to find two pairs and minimize the maximum difference within a pair.  The answer
here is 1 -- { (1, 1), (2, 3) }.  The two differences are 0 and 1, so the maximum difference
is 1.  It's the best you can do, so that's the answer.
<p>
To formulate this as a binary search problem, let's define <i>f(v)</i> as follows:
<p>
<UL>
<LI> <i>f(v)</i> is "yes" if you can find <i>p</i> distinct pairs whose 
    differences are all less than or equal to <i>v</i>.
<LI> <i>f(v)</i> is "no" if you cannot find <i>p</i> distinct pairs whose 
    differences are all less than or equal to <i>v</i>.
</UL>
It should be clear that there is a <i>v<sub>opt</sub></i> here.  Moreover, we know that there
is a value of <i>f</i> for which <i>f(v)</i> is "yes" -- it's the maximum value of the vector.
<p>
So -- continuing with the problem formualation: 
If you can implement <i>f(v)</i> in <i>O(n)</i> time (where <i>n</i> is the size of the vector),
then you can use binary search to find<i>v<sub>opt</sub></i>.  
<p>
So we need to implement <i>f(v)</i>.  To do that we can sort the vector, and the proceed
greedily.  Look at <i>nums[0]</i>.  If <i>(nums[1]-nums[0]) &le; k</i>, then we count it as
a pair and move onto <i>nums[2]</i>.  Otherwise, we ignore <i>nums[0]</i> and move onto <i>nums[1]</i>.  You can prove to yourself that if there are <i>p</i> pairs, then this algorithm will find
it.  I won't do that formally, but you should give it some thought to convince yourself that
this is true.
<p>
Let's code it up.  The Leetcode class/method is:

<p><center><table border=3 cellpadding=3><td><pre>
class Solution {
  public:
    int minimizeMax(vector &lt;int&gt; &nums, int p);
};
</pre></td></table></center><p>

In <b><a href=src/Min_The_Max_Skeleton.cpp>src/Min_The_Max_Skeleton.cpp</a></b> I have a skeleton
that reads in the array and <i>p</i> and then calls <b>minimizeMax()</b>.  As always, it compiles
and runs, but not correctly:

<pre>
UNIX> <font color=darkred><b>make bin/Min_The_Max_Skeleton</b></font>
g++ -o bin/Min_The_Max_Skeleton -Wall -Wextra -std=c++11 src/Min_The_Max_Skeleton.cpp
UNIX> <font color=darkred><b>echo 10 1 2 7 1 3  2 | bin/Min_The_Max_Skeleton </b></font>
0
UNIX> <font color=darkred><b></b></font>
</pre>

We'll program incrementally.  First, we'll write <i>f(v)</i>, which returns whether you can 
find <i>p</i> pairs, each of whose difference is &le; <i>v</i>.  It's in
<b><a href=src/Min_The_Max_Write_F.cpp>src/Min_The_Max_Write_F.cpp</a></b>:

<p><center><table border=3 cellpadding=3><td><pre>
class Solution {
  public:
    int minimizeMax(vector &lt;int&gt; &nums, int p);
    bool f(int v, const vector &lt;int&gt; &nums, int p);
};

<font color=blue>/* f(v, nums, p) returns true if there are p pairs in nums whose differences is less than
   or equal to p. */</font>

bool Solution::f(int v, const vector &lt;int&gt; &nums, int p)
{
  size_t i;
  int np;

  np = 0;
  for (i = 0; i &lt; nums.size() && np &lt; p; i++) {
    if (i &lt; nums.size()-1 && nums[i+1] - nums[i] &lt;= v) {
      np++;
      i++;
    }
  }
  return (np &gt;= p);
}

<font color=blue>/* This just lets us test f(). */</font>

int Solution::minimizeMax(vector &lt;int&gt; &nums, int p)
{
  int i;

  sort(nums.begin(), nums.end());
  for (i = 0; i &lt; 10; i++) printf("%d %s\n", i, (f(i, nums, p)) ? "yes" : "no");
  return 0;
}
</pre></td></table></center><p>

Let's test using <i>nums</i> from the first example.

<pre>
UNIX> <font color=darkred><b>make bin/Min_The_Max_Write_F</b></font>
g++ -o bin/Min_The_Max_Write_F -Wall -Wextra -std=c++11 src/Min_The_Max_Write_F.cpp
bash: bin/Min_The_Max_Skeleton: No such file or directory
UNIX> <font color=darkred><b>echo 10 1 2 7 1 3  2 | bin/Min_The_Max_Write_F</b></font>
0 no
1 yes   <font color=blue> # This is the correct answer</font>
2 yes
3 yes
4 yes
5 yes
6 yes
7 yes
8 yes
9 yes
0
UNIX> <font color=darkred><b>echo 10 1 2 7 1 3  3 | bin/Min_The_Max_Write_F</b></font>
0 no
1 no
2 no
3 yes  <font color=blue> # As is this -- (10,7), (1,2), (1,3) -- among other solutions</font>
4 yes
5 yes
6 yes
7 yes
8 yes
9 yes
0
UNIX> <font color=darkred><b></b></font>
</pre>

Now we write the binary search.  Let's consult the picture -- should we test the last
entry of the blue section, or the first entry of the pink section?

<p><center><table border=3><td><a href=jpg/start-size.jpg><img src=jpg/start-size.jpg width=500></a></td></table></center><p>

Well, if we test the last entry of the blue section, then a "yes" answer lets us discard
the pink section, and a "no" answer lets us discard the blue section.  That's what we want!
Here's the code:
(<b><a href=src/Min_The_Max_Solution.cpp>src/Min_The_Max_Solution.cpp</a></b>)

<p><center><table border=3 cellpadding=3><td><pre>
int Solution::minimizeMax(vector &lt;int&gt; &nums, int p)
{
  size_t i;

  int middle;
  int max;
  int start, size;

  sort(nums.begin(), nums.end());

  <font color=blue>/* Calculate the maximum value in nums. */</font>
  max = nums[0];
  for (i = 1; i &lt; nums.size(); i++) if (nums[i] &gt; max) max = nums[i];

  start = 0;
  size = max+1;     <font color=blue>// Since 0 is a valid answer, and we want size to be one past the</font>
                    <font color=blue>// highest valid answer, we set size to max + 1.</font>

  while (size &gt; 1) {
    middle = start + size/2 - 1;
    if (f(middle, nums, p)) {
      size = size/2;
    } else {
      start += size/2;
      size -= size/2;
    }
  }
  return start;
}
</pre></td></table></center><p>
