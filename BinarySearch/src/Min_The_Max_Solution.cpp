#include <vector>
#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;

class Solution {
  public:
    int minimizeMax(vector <int> &nums, int p);
    bool f(int v, const vector <int> &nums, int p);
};

bool Solution::f(int v, const vector <int> &nums, int p)
{
  size_t i;
  int np;

  np = 0;
  for (i = 0; i < nums.size() && np < p; i++) {
    if (i < nums.size()-1 && (nums[i+1] - nums[i]) <= v) {
      np++;
      i++;
    }
  }
  return (np >= p);
}

int Solution::minimizeMax(vector <int> &nums, int p)
{
  size_t i;

  int middle;
  int max;
  int start, size;

  sort(nums.begin(), nums.end());
  /* Calculate the maximum value in nums. */
  max = nums[0];
  for (i = 1; i < nums.size(); i++) if (nums[i] > max) max = nums[i];

  start = 0;
  size = max+1;     // Since 0 is a valid answer, and we want size to be one past the
                    // highest valid answer, we set size to max + 1.

  while (size > 1) {
    middle = start + size/2 - 1;
    if (f(middle, nums, p)) {
      size = size/2;
    } else {
      start += size/2;
      size -= size/2;
    }
  }
  return start;
}

/* Read in nums and p, and call the method. */

int main()
{
  vector <int> nums;
  int p;
  Solution s;

  while (cin >> p) nums.push_back(p);
  nums.pop_back();
  cout << s.minimizeMax(nums, p) << endl;
  return 0;
}
