#include <vector>
#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;

class Solution {
  public:
    int minimizeMax(vector <int> &nums, int p);
};

int Solution::minimizeMax(vector <int> &nums, int p)
{
  (void) nums;
  (void) p;

  return 0;
}

/* Read in nums and p, and call the method. */

int main()
{
  vector <int> nums;
  int p;
  Solution s;

  while (cin >> p) nums.push_back(p);
  nums.pop_back();
  cout << s.minimizeMax(nums, p) << endl;
  return 0;
}
