#include <vector>
#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;

class Solution {
  public:
    int minimizeMax(vector <int> &nums, int p);
    bool f(int v, const vector <int> &nums, int p);
};

bool Solution::f(int v, const vector <int> &nums, int p)
{
  size_t i;
  int np;

  np = 0;
  for (i = 0; i < nums.size() && np < p; i++) {
    if (i < nums.size()-1 && nums[i+1] - nums[i] <= v) {
      np++;
      i++;
    }
  }
  return (np >= p);
}

int Solution::minimizeMax(vector <int> &nums, int p)
{
  int i;

  sort(nums.begin(), nums.end());
  for (i = 0; i < 10; i++) printf("%d %s\n", i, (f(i, nums, p)) ? "yes" : "no");
  return 0;
}

/* Read in nums and p, and call the method. */

int main()
{
  vector <int> nums;
  int p;
  Solution s;

  while (cin >> p) nums.push_back(p);
  nums.pop_back();
  cout << s.minimizeMax(nums, p) << endl;
  return 0;
}
