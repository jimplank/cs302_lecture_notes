/* Skeleton program for writing the binary search in class. */

#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>
using namespace std;

class Bsearch {
  public:
    void Create(const string &filename);        // Create the vector from the file.
    bool Find(const string &word) const;        // Return whether a word is in the vector.
  protected: 
    vector <string> words;   
};

void Bsearch::Create(const string &filename)
{
}

bool Bsearch::Find(const string &word) const
{
  return false;
}

int main(int argc, char **argv)
{
  Bsearch b;
  bool verbose, fnd;
  int tfound, total;
  string w;

  try {
    if (argc != 3) throw (string) "usage: a.out dictionary-file verbose(y/n) -- words on stdin.";
    verbose = (argv[2][0] == 'y');

    tfound = 0;
    total = 0;
 
    b.Create(argv[1]);
    while (cin >> w) {
      total++;
      fnd = b.Find(w);
      if (fnd) tfound++;
      if (verbose) printf("%s: %s\n", w.c_str(), (fnd) ? "found" : "not-found");
    }
    printf("Found: %d of %d\n", tfound, total);
  }
 
  catch (const string &s) {
    cerr << s << endl;
    return 1;
  }
}
