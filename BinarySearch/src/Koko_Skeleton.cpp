/* Driver and skeleton solution for Leetcode problem "Koko Eating Bananas". */

#include <vector>
#include <iostream>
using namespace std;

class Solution {
public:
    int minEatingSpeed(vector<int>& piles, int h);
};

int Solution::minEatingSpeed(vector<int>& piles, int h)
{
  (void) piles;    // Silence compiler warnings
  (void) h;

  return 0;
}

int main()
{
  vector <int> p;
  int h;
  Solution s;

  while (cin >> h) p.push_back(h);   /* Read the piles and h and put them into p */
  p.pop_back();                      /* Remove h from p. */
  cout << s.minEatingSpeed(p, h) << endl;
  return 0;
}
