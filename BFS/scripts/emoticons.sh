for i in 0 1 2 3 4 5 6 ; do
  clear
  if [ $i -ge 0 ]; then echo "You currently have one emoticon typed into your cell phone"; fi
  if [ $i -ge 1 ]; then echo "You may perform one of three operations:"; fi
  if [ $i -ge 2 ]; then echo " 1. Copy what's on your screen into the clipboard."; fi
  if [ $i -ge 3 ]; then echo " 2. Paste the contents of the clipboard to the end of your screen."; fi
  if [ $i -ge 4 ]; then echo " 3. Delete one emoticon from the screen."; fi
  if [ $i -ge 5 ]; then echo "You desire to have exactly n emoticons on your screen."; fi
  if [ $i -ge 6 ]; then echo "What is the minimum number of operations that you need to achieve your desire?"; fi

  read a
done

echo "See the problem writeup for examples."
