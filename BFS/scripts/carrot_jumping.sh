for i in 0 1 2 3 4 5 6 ; do
  clear
  if [ $i -ge 0 ]; then echo "You are a rabbit who lives on the x axis."; fi
  if [ $i -ge 1 ]; then echo "You are currently at position init, which will be a number between 1 and 1,000,000,006."; fi
  if [ $i -ge 2 ]; then echo "If you are at position x, then you can jump to positions 4x+3 or 8x+7."; fi
  if [ $i -ge 3 ]; then echo "There are carrots at every multiple of 1,000,000,007."; fi
  if [ $i -ge 4 ]; then echo "You may hop a maximum of 100,000 times."; fi
  if [ $i -ge 5 ]; then echo "Return the shortest number of hops that it will take you to reach a carrot."; fi
  if [ $i -ge 6 ]; then echo "If you can't reach a carrot in 100,000 hops, then return -1."; fi

  read a
done

echo "See the problem writeup for examples."
