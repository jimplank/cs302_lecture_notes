for i in 0 1 2 3 4 5 ; do
  clear
  if [ $i -ge 0 ]; then echo "You are given a vector of up to 1000 numbers."; fi
  if [ $i -ge 1 ]; then echo "Your goal is to sort them from low to high."; fi
  if [ $i -ge 2 ]; then echo "Your only legal operation is to move a number from one place in the vector to another."; fi
  if [ $i -ge 3 ]; then echo "When you do that, all of the numbers magically move to accomodate it."; fi
  if [ $i -ge 4 ]; then echo "The price of moving the number is the number itself."; fi
  if [ $i -ge 5 ]; then echo "What's the cheapest way to sort the numbers?"; fi

  read a
done

echo "See the problem writeup for examples."
