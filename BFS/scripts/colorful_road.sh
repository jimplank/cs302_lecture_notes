for i in 0 1 2 3 4 5 6 7 8 9 10 ; do
  clear
  if [ $i -ge 0 ]; then echo "You are on a road with N segments, each of which is colored either Red, Green or Blue."; fi
  if [ $i -ge 1 ]; then echo "The road is described a by string of length N, where each character is either 'R' for Red, 'G' for Green or 'B' for blue."; fi
  if [ $i -ge 2 ]; then echo "You start at segment 0, which is always Red."; fi
  if [ $i -ge 3 ]; then echo "At any point, may jump forward i segments, but it will take i*i units of energy to do so."; fi
  if [ $i -ge 4 ]; then echo "You can only jump to a Green segment from a Red segment."; fi
  if [ $i -ge 5 ]; then echo "You can only jump to a Blue segment from a Green segment."; fi
  if [ $i -ge 6 ]; then echo "You can only jump to a Red segment from a Blue segment."; fi
  if [ $i -ge 7 ]; then echo "You can only jump forward."; fi
  if [ $i -ge 8 ]; then echo "Return the minimum amount of energy that it takes for you to get from the first segment to the last, or -1 if you cannot do so."; fi
  if [ $i -ge 9 ]; then echo "Topcoder constrains the number of segments to be from 2 to 15. "; fi
  if [ $i -ge 10 ]; then echo "I make that number 1000 in my scripts."; fi

  read a
done

echo "See the problem writeup for examples."
