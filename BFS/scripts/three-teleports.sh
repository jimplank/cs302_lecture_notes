for i in 0 1 2 3 4 ; do
  clear
  if [ $i -ge 0 ]; then echo "You are on a two-dimensional grid of squares, and can move up, down, left or right in one unit of time."; fi
  if [ $i -ge 1 ]; then echo "You start at (0,0) and want to travel to (x,y)."; fi
  if [ $i -ge 2 ]; then echo "There are three teleports.  A teleport is defined by (x1,y1) and (x2,y2)."; fi
  if [ $i -ge 3 ]; then echo "When you reach one of the teleport's points, you can get to the other through the teleport in 10 units of time".; fi
  if [ $i -ge 4 ]; then echo "Return the fastest way to get to (x,y)".; fi
  read a
done
