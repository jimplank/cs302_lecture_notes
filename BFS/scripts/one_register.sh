for i in 0 1 2 3 4 5 6 ; do
  clear
  if [ $i -ge 0 ]; then echo "You have a really cheap computer that has one register."; fi
  if [ $i -ge 1 ]; then echo "You are given the starting value of the register."; fi
  if [ $i -ge 2 ]; then echo "You are given a target value that you would like to set the register to."; fi
  if [ $i -ge 3 ]; then echo "The only operations you can do on the register are +, -, * and /"; fi
  if [ $i -ge 4 ]; then echo "The + operation adds the register's value to itself, and sets the register to that value.."; fi
  if [ $i -ge 5 ]; then echo "The -, * and / operations work in the same way."; fi
  if [ $i -ge 6 ]; then echo "HERE"; fi

  read a
done

echo "See the problem writeup for examples."
