#include "colorful_bricks.hpp"
#include <iostream>
using namespace std;

/* This is a control implementation that does nothing.  We use this
   to time the scaffolding of the program so that we may properly
   evaluate how fast the other programs run. */

int ColorfulBricks::countLayouts(const string &bricks)
{
  (void) bricks;   
  return 0;
}
