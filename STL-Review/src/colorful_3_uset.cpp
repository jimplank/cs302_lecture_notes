/* This is identical to colorful_2_set.cpp, except we use
   an unordered set rather than a set.  This should be
   signifcantly faster. */

#include "colorful_bricks.hpp"
#include <string>
#include <unordered_set>
#include <iostream>
using namespace std;

int ColorfulBricks::countLayouts(const string &bricks)
{
  size_t i;
  unordered_set <char> s;

  for (i = 0; i < bricks.size(); i++) s.insert(bricks[i]);
  if (s.size() == 1) return 1;
  if (s.size() == 2) return 2;
  return 0;
}
