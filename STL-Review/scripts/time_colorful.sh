all="bin/colorful_1_sort bin/colorful_2_set bin/colorful_3_uset bin/colorful_4_map bin/colorful_5_vec bin/colorful_6_vec bin/colorful_7_two_chars"
control=bin/colorful_0_control

for i in $all $control ; do
  if [ ! -f $i ]; then
    make $i
  fi
done

ctime=`( time sh -c "sh scripts/test_colorful_bricks.sh $control > /dev/null" ) 2>&1 | grep user | sed 's/user//' | sed 's/0m//' | sed s/s// `

for i in $all ; do
  utime=`( time sh -c "sh scripts/test_colorful_bricks.sh $i> /dev/null" ) 2>&1 | grep user | sed 's/user//' | sed 's/0m//' | sed s/s// `

  echo $i $utime $ctime | awk '{ print $1, $2-$3 }'
done
