clear
for i in 1 2 3 4 5 6 7 8 ; do
  read a
  clear
  if [ $i -ge 1 ]; then
     echo 'You are implementing a class called ColorfulBricks with a method called countLayouts().' 
  fi
  if [ $i -ge 2 ]; then
     echo ""
     echo CountLayouts is given a string called bricks composed of upper-case letters. 
  fi
  if [ $i -ge 3 ]; then
     echo ""
     echo "The topcoder description limits bricks to 50 characters, but we'll do 1,000,000."
  fi
  if [ $i -ge 4 ]; then
     echo ""
     echo 'A string is "nice" if the characters are arranged such that there is '
     echo at most one pair of adjacent characters that are different. 
  fi
  if [ $i -ge 5 ]; then
     echo ""
     echo You are to return the number of ways that you can arrange 
     echo the characters of bricks into nice strings. 
  fi
  if [ $i -ge 6 ]; then
     echo ""
     echo Example 0: bricks = "ABAB".  Answer = 2
  fi
  if [ $i -ge 7 ]; then
     echo ""
     echo Example 1: bricks = "JJJJJ".  Answer = 1
  fi
  if [ $i -ge 8 ]; then
     echo ""
     echo Example 1: bricks = "COSC".  Answer = 0
  fi
done
