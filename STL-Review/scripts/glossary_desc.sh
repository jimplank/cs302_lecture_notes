clear

open img/gloss_ex1.jpg

for i in 1 2 3 4 5 6 7 8 9 ; do
  read a
  clear
  if [ $i -ge 1 ]; then
     echo 'You are given a vector of strings. We call each entry of the vector a "term."'
  fi
  if [ $i -ge 2 ]; then
     echo ""
     echo 'You want to make a formatted glossary of the terms, which will be a vector of strings'
  fi
  if [ $i -ge 3 ]; then
     echo ""
     echo "In the glossary, the terms will be sorted alphabetically, ignoring case."
  fi
  if [ $i -ge 4 ]; then
     echo ""
     echo 'The glossary will have two columns. Column 1 for A-M, Column 2 for N-Z.'
  fi
  if [ $i -ge 5 ]; then
     echo ""
     echo Each column has exactly 19 characters, padded with spaces.
     echo The columns are separated by two spaces.
  fi
  if [ $i -ge 6 ]; then
     echo ""
     echo "If there is a term starting with a letter, "
     echo "then before all of the terms with that letter,"
     echo "there will be a line containing just the upper case letter, "
     echo "and then a line of 19 dashes."
  fi
  if [ $i -ge 7 ]; then
     echo ""
     echo "The terms themselves are printed with two leading spaces."
  fi
  if [ $i -ge 8 ]; then
     echo ""
     echo "If the columns are not the same size, you pad the shorter one with lines of 19 spaces."
  fi
  if [ $i -eq 10 ]; then open img/gloss_ex2.jpg; fi
done
