<title>CS302 Lecture Notes - Dynamic Programming </title>
<h2>CS302 Lecture Notes - Dynamic Programming<br>Example Program #1: Fibonacci Numbers</h2>
<UL>
<LI> November 18, 2009
<LI> Latest update: November, 2020
<LI> James S. Plank
<LI> Directory: <b>/home/jplank/cs302/Notes/DynamicProgramming</b>
</UL>
<hr>

You can make the programs in this lecture with "<b>make fib</b>".
<p>
This is one of the simplest and cleanest dynamic programming problems.
<p>
Fibonacci numbers have a recursive definition:
<p>
<center>
Fib(0) = 1<br>
Fib(1) = 1</br>
If <i>n</i> > 1, Fib(<i>n</i>) =
Fib(<i>n-1</i>) +
Fib(<i>n-2</i>)
</center>
<p>


<hr>
<h3> Step One </h3>

This definition maps itself to a simple recursive function, which you've seen before in CS202.  
However,
we'll go through it again.  This is <b>Step 1</b>: writing a recursive answer to a problem.
I bundle this into a class because it makes steps 2 and 3 easier.
It's in 
<b><a href=../src/fib1.cpp>src/fib1.cpp</a></b>:

<p><center><table border=3 cellpadding=3><td><pre>
#include &lt;cstdlib&gt;
#include &lt;iostream&gt;
using namespace std;

class Fib {               <font color=blue>// Step 1 in calculating Fibonacci numbers with Dynamic Programming:</font>
  public:                 <font color=blue>// Find a recursive solution, which may be very slow.</font>
    long long find_fib(long long n);
};

long long Fib::find_fib(long long n)  <font color=blue>// Classic recursive implementation straight from the definition.</font>
{
  if (n == 0 || n == 1) return 1;
  return find_fib(n-1) + find_fib(n-2);
}

int main(int argc, char **argv)
{
  Fib f;

  if (argc != 2) { cerr &lt;&lt; "usage: fib n\n"; exit(1); }

  cout &lt;&lt; f.find_fib(atoi(argv[1])) &lt;&lt; endl;
  return 0;
}
</pre></td></table></center><p>



The problem with this is that its performance  blows up exponentially:

<UL>
<LI> <b>find_fib(n)</b> calls <b>find_fib(n-1)</b> and <b>find_fib(n-2)</b>.
<LI> <b>find_fib(n-1)</b> calls <b>find_fib(n-2)</b> and <b>find_fib(n-3)</b>.
<LI> <b>find_fib(n-2)</b>, which is called twice, calls <b>find_fib(n-3)</b> and <b>find_fib(n-4)</b>.
<LI> <b>find_fib(n-3)</b>, which is called three times, calls <b>find_fib(n-4)</b> and <b>find_fib(n-5)</b>.
<LI> <b>find_fib(n-4)</b>, which is called five times, calls <b>find_fib(n-5)</b> and <b>find_fib(n-6)</b>.
<LI> <b>find_fib(n-5)</b>, which is called eight times, calls <b>find_fib(n-6)</b> and <b>find_fib(n-7)</b>.
</UL>

If you continue with the pattern, what you'll see is that <b>find_fib(n-i)</b> is called
<b>Fib(i)</b> times.  And the Fibonacci numbers blow up exponentially.  
When you run <b>bin/fib1</b>, you'll see it start to bog down when <i>n</i> gets to the
40's:

<pre>
UNIX> <font color=darkred><b>bash</b></font>
UNIX> <font color=darkred><b>i=0</b></font>
UNIX> <font color=darkred><b>while [ $i -lt 50 ]; do</b></font>
> <font color=darkred><b>echo $i `( time bin/fib1 $i ) 2>&1`</b></font>    <font color=blue> # This squashes the output of "time" onto one line.</font>
> <font color=darkred><b>i=$(($i+1))</b></font>                            <font color=blue> # You can ask me about it class or wait until CS360.</font>
> <font color=darkred><b>done</b></font>
0 1 real 0m0.003s user 0m0.001s sys 0m0.001s
1 1 real 0m0.003s user 0m0.001s sys 0m0.001s
                                         <font color=blue> # .......  Skipping lines</font>
40 165580141 real 0m0.272s user 0m0.270s sys 0m0.001s
41 267914296 real 0m0.439s user 0m0.437s sys 0m0.001s
42 433494437 real 0m0.697s user 0m0.694s sys 0m0.001s
43 702028733 real 0m1.114s user 0m1.111s sys 0m0.001s
44 1134903170 real 0m1.812s user 0m1.809s sys 0m0.002s
45 1836311903 real 0m2.934s user 0m2.931s sys 0m0.002s
46 2971215073 real 0m4.756s user 0m4.752s sys 0m0.002s
47 4807526976 real 0m7.800s user 0m7.790s sys 0m0.005s
48 7778742049 real 0m13.135s user 0m13.103s sys 0m0.014s
49 12586269025 real 0m21.007s user 0m20.971s sys 0m0.016s
UNIX> <font color=darkred><b></b></font>
</pre>


<hr>
<h3> Step Two </h3>

When we teach this in CS202, we turn the recursion into a <b>for()</b> loop that starts with Fib(0)
and Fib(1) and builds up to Fib(<i>n</i>).  
<p>
However, with dynamic programming, we proceed to step two: memoization.  
We accept the recursive definition, but simply create a cache for the
answers to that after the first time <b>find_fib(<i>i</i>)</b> is called for some <i>i</i>,
it returns its answer from the cache.  
We implement it in 
<b><a href=../src/fib2.cpp>src/fib2.cpp</a></b> below.  We initialize the cache with -1's to 
denote empty values.

<p><center><table border=3 cellpadding=3><td><pre>
class Fib {  <font color=blue>// This is step 2 of dynamic programming: Add a cache.</font>
  public:
    long long find_fib(int n);
    vector &lt;long long&gt; cache;
};

long long Fib::find_fib(int n)   <font color=blue>// Create the cache if this is the first call.</font>
{                                <font color=blue>// Otherwise, return the answer from the cache if it's there.</font>
  if (cache.size() == 0) cache.resize(n+1, -1);
  if (cache[n] != -1) return cache[n];

  if (n == 0 || n == 1) {        <font color=blue>// If it's not in the cache, do the recursion, and put</font>
    cache[n] = 1;                <font color=blue>// the answer into the cache before returning.</font>
  } else {
    cache[n] = find_fib(n-1) + find_fib(n-2);
  }
  return cache[n];
}
</pre></td></table></center><p>


Now <b>find_fib(n)</b> is linear in <i>n</i>, which is a HUGE difference.  The call that took
21 seconds below takes a couple of milliseconds:

<pre>
UNIX> <font color=darkred><b>time bin/fib2 49</b></font>
12586269025

real	0m0.003s
user	0m0.001s
sys	0m0.001s
UNIX> <font color=darkred><b></b></font>
</pre>

<hr>
<h3> Step Three </h3>

Next, we perform <b>Step 3</b>, which makes the observation that whenever we call <b>find_fib(<i>n</i>)</b>,
it only makes recursive calls to values less than <i>n</i>.  That means that we can build the cache from zero
up to <i>n</i> with a <b>for</b> loop
(<b><a href=../src/fib3.cpp>src/fib3.cpp</a></b>):

<p><center><table border=3 cellpadding=3><td><pre>
class Fib {              <font color=blue>// This is step three -- remove the recursion and build the cache.</font>
  public:
    long long find_fib(int n);
    vector &lt;long long&gt; cache;
};

long long Fib::find_fib(int n)
{
  int i;
  if (n == 0 || n == 1)  return 1;

  cache.resize(n+1, -1);

  <font color=blue>/* Because all of our recursive calls were to smaller values of n, we can
     build the cache from small to big. */</font>

  cache[0] = 1;
  cache[1] = 1;
  for (i = 2; i &lt;= n; i++) cache[i] = cache[i-1] + cache[i-2];

  return cache[n];
}
</pre></td></table></center><p>

It's not appreciably faster, because <b>fib2</b> is already smoking fast, but it does
demonstrate step three:

<pre>
UNIX> <font color=darkred><b>time bin/fib3 49</b></font>
12586269025

real	0m0.003s
user	0m0.001s
sys	0m0.001s
UNIX> <font color=darkred><b></b></font>
</pre>

<hr>
<h3>Step Four</h3>

Finally, when we reflect that we only ever look at the last two values in the
cache, we can turn the cache into just two elements..  This is <b>Step 4</b> (in
<b><a href=../src/fib4.cpp>src/fib4.cpp</a></b>).  Again, when we teach this in CS202, we
reduce the program to three variables: the values for <i>n</i>, <i>n-1</i> and <i>n-2</i>.
What I'm going to do here, though is keep the cache as a vector, but now with just two
elements.  <b>Fib(n)</b> for even values of <i>n</i> will go into <b>cache[0]</b>, and
odd values of <i>n</i> will go into <b>cache[1]</b>.  In the end, we return the proper value
of the cache, depending on whether <i>n</i> is even or odd.
<p>
Keep this technique in mind -- sometimes you can do step 4 simply by looking at how you access
the cache in step 3.

<p><center><table border=3 cellpadding=3><td><pre>
class Fib {    <font color=blue>// Step four: removing (or minimizing) the cache.</font>
  public:
    long long find_fib(int n);
};

long long Fib::find_fib(int n)
{
  vector &lt;long long&gt; cache;
  int i;

  if (n == 0 || n == 1) return 1;

  <font color=blue>/* The key observation here is that we only need the last two entries of the cache.
     So, we'll limit the cache to two entries.  We'll keep the odd ones in cache[0],
     and the even ones in cache[1]. */</font>
     
  cache.resize(2, 1);
  for (i = 2; i &lt;= n; i++) {
    cache[i%2] = cache[0] + cache[1];
  }

  <font color=blue>/* Return cache[0] if n is even, and cache[1] if n is odd. */</font>

  return cache[n%2];
}
</pre></td></table></center><p>
