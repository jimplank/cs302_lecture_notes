<title>CS302 Lecture Notes - Dynamic Programming </title>
<h2>CS302 Lecture Notes - Dynamic Programming<br>Example Program #3: The Maximum Subsequence Problem</h2>
<UL>
<LI> November 18, 2009
<LI> Latest revision: November, 2020.
<LI> James S. Plank
<LI> Directory: <b>/home/jplank/cs302/Notes/DynamicProgramming</b>
</UL>
<hr>

This problem also appears as 
<a href=https://leetcode.com/problems/longest-common-subsequence/>
Leetcode problem #1143.</a>
<p>
This one originally comes from a topcoder article by
<a href=http://www.topcoder.com/tc?module=Static&d1=features&d2=040104>vorthys</a>.
You are to write a program that takes two strings and returns the length of the largest
common subsequence of the two strings.  A subsequence of a string is any string that you
can create by deleting one or more letters of the original string.
For example, "dog" is a subsequence
if "dodger", but "red" is not.  Even though the characters appear in "dodger", you cannot
create "red" by deleting characters from "dodger."  
<p>
You can compile the programs with "make subseq".

<hr>
<h3>Step One</h3>

As always, we start with <b>Step 1</b>, which is to spot the recursive solution.  To do so,
let's take a look at two examples.  Here's the first example:
<p>
<center>
S1 = "AbbbcccFyy", S2 = "FxxxyyyAc"
<p>
</center>
And here's the second example.
<p>
<center>
S1 = "AbbbcccFyy", S2 = "FxxxyyyAccc"
</center>
<p>
You should see that these examples are related.  In the first, the longest subsequence is "Fyy",
and in the second, the longest subsequence is "Accc".  This example highlights the difficulty
of this problem -- If you start looking for subsequences from the beginning of either string,
the result may be affected by the end of the string.  We'll return to these examples in a bit.
Let's try another example:
<p>
<center>
S1 = "9009709947053769973735856707811337348914", S2 = "9404888367379074754510954922399291912221"
</center>
<p>
Man, that looks hard.  Don't even bother trying to solve it.  However, one thing you do know --
the maximum subsequence starts with a 9.  Why?  It has to -- you can prove this to yourself
by contradiction: If the subsequence is <i>S</i> and <i>S</i> doesn't start with 9, then there is
a valid subsequence which is "9"+<i>S</I>.  
<p>
I'm hoping these examples help you spot the recursion:  You will compare the first letters of
both substrings, and do different things depending on whether they equal each other:
<p>
<UL>
<LI> If <b>S1[0] == S2[0]</b>, then the answer is one plus the maximum subsequence of
<b>S1.substr(1)</b> and 
<b>S2.substr(1)</b>.  (This method returns the substring starting at index 1 and going to
the end of the string).
<p><LI> If <b>S1[0] != S2[0]</b>, then you know that the maximum subsequence either does
not start with <b>S1[0]</b> or does not start with <b>S2[0]</b>.  Thus, you should
determine 
the maximum subsequence of <b>S1</b> and <b>S2.substr(1)</b>, and 
the maximum subsequence of <b>S2</b> and <b>S1.substr(1)</b>.  
The answer has to be one of these.
</UL>
<p>
Let's hack that up.  It's in 
<b><a href=../src/subseq1.cpp>src/subseq1.cpp</a></b> and is straightforward:

<p><center><table border=3 cellpadding=3><td><pre>
int Subseq::MCS(const string &s1, const string &s2)
{
  int r1, r2;

  <font color=blue>/* Base case -- if either string is empty, return 0. */</font>

  if (s1.size() == 0 || s2.size() == 0) return 0;

  <font color=blue>/* If the first characters of the two strings equal each other,
     then the answer is one plus the MCS of the two string with
     the first characters chopped off. */</font>

  if (s1[0] == s2[0]) return 1 + MCS(s1.substr(1), s2.substr(1));

  <font color=blue>/* Otherwise, the answer is either:
       - r1: The MCS of the 1st string, and the 2nd string without its first character
       - r2: The MCS of the 2nd string, and the 1st string without its first character
   */</font>   

  r1 = MCS(s1, s2.substr(1));
  r2 = MCS(s1.substr(1), s2);
  return (r1 &gt; r2) ? r1 : r2;   <font color=blue>// Return the maximum of r1 and r2.</font>
}
</pre></td></table></center><p>

It seems to work on our examples:

<pre>
UNIX> <font color=darkred><b>bin/subseq1 AbbbcccFyy FxxxyyyAc</b></font>
3
UNIX> <font color=darkred><b>bin/subseq1 AbbbcccFyy FxxxyyyAccc</b></font>
4
UNIX> <font color=darkred><b>bin/subseq1 9009709947053769973735856707811337348914 9404888367379074754510954922399291912221</b></font>
<font color=darkred><b>&lt;CNTL-C&gt;</b></font>
UNIX> <font color=darkred><b></b></font>
</pre>

However, it hangs on that last one.  Why?  Exponential blow-up of duplicate calls, of course.
So, let's move to Step Two:

<hr>
<h3> Step Two</h3>

This is an easy memoization on the strings -- just concatenate them with a 
character that can't be in either string -- I'll choose a colon here.  Now, I'm 
going to turn the solution into a class so that storing the cache is easier.  
It's in <b><a href=../src/subseq2.cpp>src/subseq2.cpp</a></b>:

<p><center><table border=3 cellpadding=3><td><pre>
class Subseq {
  public:
    map &lt;string, int&gt; cache;
    int MCS(const string &s1, const string &s2);
};

int Subseq::MCS(const string &s1, const string &s2)
{
  string key;    <font color=blue>// We'll turn the arguments into a string for the cache.</font>
  int r1, r2;

  <font color=blue>/* Take care of the base cases without worrying about the cache. */</font>

  if (s1.size() == 0 || s2.size() == 0) return 0;

  <font color=blue>/* Create the memoization key, and check the cache. */</font>

  key = s1 + ":";
  key += s2;
  if (cache.find(key) != cache.end()) return cache[key];

  <font color=blue>/* If it's not in the cache, then do the recursion and put the answer into the cache. */</font>

  if (s1[0] == s2[0]) {
   cache[key] = 1 + MCS(s1.substr(1), s2.substr(1));
  } else {
    r1 = MCS(s1, s2.substr(1));
    r2 = MCS(s1.substr(1), s2);
    cache[key] = (r1 &gt; r2) ? r1 : r2;
  }
  return cache[key];
}
</pre></td></table></center><p>


This works fine on our examples:

<pre>
UNIX> <font color=darkred><b>bin/subseq2 AbbbcccFyy FxxxyyyAc</b></font>
3
UNIX> <font color=darkred><b>bin/subseq2 AbbbcccFyy FxxxyyyAccc</b></font>
4
UNIX> <font color=darkred><b>time bin/subseq2 9009709947053769973735856707811337348914 9404888367379074754510954922399291912221</b></font>
15

real	0m0.007s
user	0m0.004s
sys	0m0.002s
UNIX> <font color=darkred><b></b></font>
</pre>

However, this solution should make you feel uneasy.  It makes me feel uneasy.  The reason is
that each memoization key makes a copy of the two strings, and each call to <b>substr()</b>
creates a new string which is just one character smaller than the previous string.  That's
a lot of memory.  To hammer home this point, the file
<b><a href=txt/sub-big.txt>txt/sub-big.txt</a></b>
has two 3000-character strings.  When we call <b>bin/subseq2</b> on it, it hangs because it is making
so many copies of large strings:

<pre>
UNIX> <font color=darkred><b>bin/subseq2 `cat txt/sub-big.txt`</b></font>
</pre>

Think about it -- each substring is a suffix of the previous one, so for S1 and S2 there are
roughly 3000 suffixes.  That means potentially 3000 * 3000 calls to <b>MCS()</b>, each time 
creating strings of size up to 3000, and memoization keys of size up to 6000.
That's a huge amount of time and memory.  
<p>
Since we are only using suffixes of S1 and S2, we can represent them with indices to their
first characters, and we call <b>MCS()</b> on these indices rather than on the strings.
I've put that code into <b><a href=../src/subseq3.cpp>src/subseq3.cpp</a></b>.  It doesn't memoize.

<p><center><table border=3 cellpadding=3><td><pre>
class Subseq {
  public:
    string s1, s2;
    int MCS(size_t i1, size_t i2);
};

<font color=blue>/* This code doesn't memoize -- it's like the step 1 code that worked on strings,
   only now we're working on indices, and not making any copies of strings. */</font>

int Subseq::MCS(size_t i1, size_t i2)
{
  int r1, r2;

  if (s1.size() == i1 || s2.size() == i2) return 0;

  if (s1[i1] == s2[i2]) {
    return 1 + MCS(i1+1, i2+1);
  } else {
    r1 = MCS(i1, i2+1);
    r2 = MCS(i1+1, i2);
    return (r1 &gt; r2) ? r1 : r2;
  }
}
</pre></td></table></center><p>

It works, although since it doesn't memoize, it hangs on the long input.

<pre>
UNIX> <font color=darkred><b>bin/subseq3 AbbbcccFyy FxxxyyyAc</b></font>
3
UNIX> <font color=darkred><b>bin/subseq3 AbbbcccFyy FxxxyyyAccc</b></font>
4
UNIX> <font color=darkred><b>bin/subseq3 9009709947053769973735856707811337348914 9404888367379074754510954922399291912221</b></font>
</pre>

We memoize in <b><a href=../src/subseq4.cpp>src/subseq4.cpp</a></b>.  Our cache is on the 
indices, so it's a two-dimensional vector, and is of size <b>s1.size() * s2.size()</b>:

<p><center><table border=3 cellpadding=3><td><pre>
class Subseq {
  public:
    string s1, s2;
    vector &lt; vector &lt;int&gt; &gt; cache;      <font color=blue>// Here's our memoization cache.</font>
    int MCS(size_t i1, size_t i2);
};

int Subseq::MCS(size_t i1, size_t i2)
{
  int r1, r2;
  size_t i;

  <font color=blue>/* Base case. */</font>

  if (s1.size() == i1 || s2.size() == i2) return 0;

  <font color=blue>/* Create the cache if necessary -- sentinelize with -1. */</font>

  if (cache.size() == 0) {
    cache.resize(s1.size());
    for (i = 0; i &lt; s1.size(); i++) cache[i].resize(s2.size(), -1);
  }

  <font color=blue>/* If the answer is in the cache, return it. */</font>

  if (cache[i1][i2] != -1) return cache[i1][i2];

  <font color=blue>/* Otherwise, calculate it with recursion. */</font>

  if (s1[i1] == s2[i2]) {
   cache[i1][i2] = 1 + MCS(i1+1, i2+1);
  } else {
    r1 = MCS(i1, i2+1);
    r2 = MCS(i1+1, i2);
    cache[i1][i2] = (r1 &gt; r2) ? r1 : r2;
  }
  return cache[i1][i2]; 
}
</pre></td></table></center><p>

Now this version is fast, and it even works on the huge input in under a second!

<pre>
UNIX> <font color=darkred><b>bin/subseq4 AbbbcccFyy FxxxyyyAc</b></font>
3
UNIX> <font color=darkred><b>bin/subseq4 AbbbcccFyy FxxxyyyAccc</b></font>
4
UNIX> <font color=darkred><b>bin/subseq4 9009709947053769973735856707811337348914 9404888367379074754510954922399291912221</b></font>
15
UNIX> <font color=darkred><b>time bin/subseq4 `cat txt/sub-big.txt`</b></font>
891

real	0m0.123s
user	0m0.110s
sys	0m0.012s
UNIX> <font color=darkred><b></b></font>
</pre>

<hr>
<h3>Step 3</h3>

We can perform step #3 on this by realizing that you perform the recursion on larger indices,
so you build the cache from high to low, rather than low to high in the Fibonacci 
and Coin programs.  This is in
<b><a href=../src/subseq5.cpp>src/subseq5.cpp</a></b>:

<p><center><table border=3 cellpadding=3><td><pre>
int Subseq::MCS()
{
  int r1, r2, i1, i2;
  size_t i;

  <font color=blue>/* Create the cache, and fill it with zero's. 
     You'll note that cache[i][s2.size()] and cache[s1.size()][i] should equal zero,
     so we don't have to calculate them. */</font>

  cache.resize(s1.size()+1);
  for (i = 0; i &lt; cache.size(); i++) cache[i].resize(s2.size()+1, 0);

  <font color=blue>/* Now start with i1 and i2 being s1.size()-1 and s2.size()-1 respectively, and 
     fill in the cache from high to low.  */</font>

  for (i1 = (int) s1.size()-1; i1 &gt;= 0; i1--) {
    for (i2 = (int) s2.size()-1; i2 &gt;= 0; i2--) {
      if (s1[i1] == s2[i2]) {
       cache[i1][i2] = 1 + cache[i1+1][i2+1];
      } else {
        r1 = cache[i1][i2+1];
        r2 = cache[i1+1][i2];
        cache[i1][i2] = (r1 &gt; r2) ? r1 : r2;
      }
    }
  }

  <font color=blue>/* The final answer is in cache[0][0]. */</font>

  return cache[0][0]; 
}
</pre></td></table></center><p>

This is much faster than the previous code:

<pre>
UNIX> <font color=darkred><b>time bin/subseq5 `cat txt/sub-big.txt`</b></font>
891

real	0m0.039s
user	0m0.026s
sys	0m0.011s
UNIX> <font color=darkred><b></b></font>
</pre>

<hr>
<h3>Step 4</h3>

Finally, we can perform step 4 on this by only keeping two rows, and performing 
the arithmetic on <i>i1</i> mod 2.  Now, the cache only contains <b>(2*s2.size())</b> 
entries.  The
code is in 

<p><center><table border=3 cellpadding=3><td><pre>
int Subseq::MCS()
{
  int r1, r2, i, i1, i2, index, other;

  cache.resize(2);
  for (i = 0; i &lt; cache.size(); i++) cache[i].resize(s2.size()+1, 0);

  for (i1 = s1.size()-1; i1 &gt;= 0; i1--) {
    index = i1%2;
    other = (i1+1)%2;
    for (i2 = s2.size()-1; i2 &gt;= 0; i2--) {
      if (s1[i1] == s2[i2]) {
       cache[index][i2] = 1 + cache[other][i2+1];
      } else {
        r1 = cache[index][i2+1];
        r2 = cache[other][i2];
        cache[index][i2] = (r1 &gt; r2) ? r1 : r2;
      }
    }
  }
  return cache[0][0]; 
}
</pre></td></table></center><p>

It runs a little faster than before, presumably because it's using less memory:

<pre>
UNIX> <font color=darkred><b>time bin/subseq6 `cat txt/sub-big.txt`</b></font>
891

real	0m0.026s
user	0m0.021s
sys	0m0.003s
UNIX> <font color=darkred><b></b></font>
</pre>

<hr>
<h3>When Step 1 is faster than the others</h3>

Here's a curiosity -- take a look at the timings and the outputs 
and see if you can figure out what's going on:

<pre>
UNIX> <font color=darkred><b>time bin/subseq3 `cat txt/sub-big-2.txt`</b></font>                <font color=blue> # Step 1</font>
2592

real	0m0.007s
user	0m0.003s
sys	0m0.003s
UNIX> <font color=darkred><b>time bin/subseq4 `cat txt/sub-big-2.txt`</b></font>                <font color=blue> # Step 2</font>
2592

real	0m0.021s
user	0m0.009s
sys	0m0.009s
UNIX> <font color=darkred><b>time bin/subseq5 `cat txt/sub-big-2.txt`</b></font>                <font color=blue> # Step 3</font>
2592

real	0m0.038s
user	0m0.025s
sys	0m0.011s
UNIX> <font color=darkred><b>time bin/subseq6 `cat txt/sub-big-2.txt`</b></font>                <font color=blue> # Step 4</font>
2592

real	0m0.021s
user	0m0.017s
sys	0m0.003s
UNIX> <font color=darkred><b>wc txt/sub-big-2.txt</b></font>                                   <font color=blue> # The input is two strings of size 2592.</font>
       2       2    5186 txt/sub-big-2.txt
UNIX> <font color=darkred><b>sort -u txt/sub-big-2.txt | wc</b></font>                         <font color=blue> # And the two strings are identical.</font>
       1       1    2593
UNIX> <font color=darkred><b></b></font>
</pre>

What is happening?  Well, since the strings are identical, the recursive version always
calls <b>MCS(i1+1,i2+1)</b>.  There is no exponential blow-up, and the answer is found
in 2593 recursive calls.  It's fast!
<p>
Now, in steps 2 and 3, they both employ a cache of size 2593*2593.  In step 2, creating that
cache dominates the running time, because like step 1, it only makes 2593 recursive calls.
In step three, once you make the cache, you still have to fill it in -- <i>every element.</i>
That's why it takes so long.
<p>
In step 4, we're still filling in 2593*2953 cache entries, but our cache's size is only
2593*2.  That's why it's faster than step 3, but it doesn't gain significantly over step 2,
and is still inferior to step 1.  
