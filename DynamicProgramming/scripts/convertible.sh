for i in 0 1 2 3 4 5 6 7 8 9 ; do
  clear
  if [ $i -ge 0 ]; then echo "You are given two strings composed of letters A through I."; fi
  if [ $i -ge 1 ]; then echo "Call these strings X and Y."; fi
  if [ $i -ge 2 ]; then echo "Their size is <= 50"; fi
  if [ $i -ge 3 ]; then echo "Your goal is to choose some number of indices, and"; fi
  if [ $i -ge 3 ]; then echo "    delete characters in both strings with those indices."; fi
  if [ $i -ge 4 ]; then echo "When you're done, you should be able to convert X to Y with a substitution cipher."; fi
  if [ $i -ge 5 ]; then echo "What is the minimum number of characters that you have to delete."; fi
  if [ $i -ge 6 ]; then echo ""; fi
  if [ $i -ge 6 ]; then echo 'Example 1: X = "DEF" and Y = "FEG" - Answer = 0.'; fi
  if [ $i -ge 7 ]; then echo 'Example 2: X = "AAA" and Y = "BCD" - Answer = 2.'; fi
  if [ $i -ge 8 ]; then echo 'Example 3: X = "DEFDEDFFDEED" and Y = "WYZYXYWYZYXY" - Answer = 6.'; fi
  if [ $i -ge 9 ]; then echo 'Example 4: X = "ABACDCECDCDAAABBFBEHBDFDDHHD" and Y = "GBGCDCECDCHAAIBBFHEBBDFHHHHE" - Answer = 9.'; fi
  read a
done

