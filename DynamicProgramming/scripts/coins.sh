for i in 0 1 2 3 4 ; do
  clear
  if [ $i -ge 0 ]; then echo "You have N denominations of stamps, valued V1, V2, ..., VN"; fi
  if [ $i -ge 1 ]; then echo "You have an infinite number of each denomination."; fi
  if [ $i -ge 2 ]; then echo "You have a total amount of postage you need, which is S"; fi
  if [ $i -ge 3 ]; then echo "What is the smallest number stamps you can use for your postage?"; fi
  if [ $i -ge 4 ]; then echo "N <= 50.  S <= 10000."; fi

  read a
done

