for i in 0 1 2 3 4 5 6 ; do
  clear
  if [ $i -ge 0 ]; then echo "You have two strings, S1 and S2 of size <= 2600"; fi
  if [ $i -ge 1 ]; then echo "The strings are composed of upper and lower case letters."; fi
  if [ $i -ge 2 ]; then echo "X is a subsequence of Y, if you can create X by deleting characters from Y"; fi
  if [ $i -ge 3 ]; then echo "What is the longest common subsequence of S1 and S2?"; fi
  if [ $i -ge 4 ]; then echo ""; fi
  if [ $i -ge 4 ]; then echo 'Example 1: S1 = "AbbbcccFyy", S2 = "FxxxyyyAc"'; fi
  if [ $i -ge 5 ]; then echo 'Example 2: S1 = "AbbbcccFyy", S2 = "FxxxyyyAccc"'; fi
  if [ $i -ge 6 ]; then echo ""; fi
  if [ $i -ge 6 ]; then echo 'Example 3: S1 = "9009709947053769973735856707811337348914"'; fi
  if [ $i -ge 6 ]; then echo '           S2 = "9404888367379074754510954922399291912221"'; fi
  read a
done

