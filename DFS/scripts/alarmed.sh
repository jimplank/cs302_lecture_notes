for i in 0 1 2 3 5 ; do
  clear
  if [ $i -ge 0 ]; then echo "You have a room with dimensions 1000 * 1000.  There is a door at (500,0) and (500,1000)."; fi
  if [ $i -ge 1 ]; then echo "The room has up to 50 intruder-detecting alarms.  Each alarm has an (x,y) coordinate and a radius r."; fi
  if [ $i -ge 2 ]; then echo "When you power the alarms with p, the radius of detection is pr, meaning:"; fi
  if [ $i -ge 3 ]; then echo "  If an intruder is within a radius of pr  of the alarm, the alarm will catch the intruder."; fi
  if [ $i -ge 4 ]; then echo "Return the minimum value of p which will prevent an intruder from entering one door and exiting the other undetected."; fi
  if [ $i -ge 5 ]; then echo "Your answer has to be correct to a tolerance of 0.000000001."; fi
  read a
done
