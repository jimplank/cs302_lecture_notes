for i in 0 1 2 3 4 ; do
  clear
  if [ $i -ge 0 ]; then echo "There are N cities, and you want to travel to each of them exactly once."; fi
  if [ $i -ge 1 ]; then echo "You can travel from any city to any city; however, there are some roads that you *must* take."; fi
  if [ $i -ge 2 ]; then echo "Those roads are specified in an adjacency matrix."; fi
  if [ $i -ge 3 ]; then echo "Return the number of possible paths that you can take (A path is an ordering of cities)".; fi
  if [ $i -ge 4 ]; then echo "Return the number modulo 1,000,000,007".; fi
  read a
done

echo "See the problem writeup for examples."
