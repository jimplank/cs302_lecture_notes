for i in 0 1 2 3 4 5 6 7 8 9 ; do
  clear
  if [ $i -ge 0 ]; then echo "Standard definition of a subsequence."; fi
  if [ $i -ge 1 ]; then echo "Define maximum increasing subsequence."; fi
  if [ $i -ge 2 ]; then echo "You are given a vector of integers of numbers between 0 and 10^9."; fi
  if [ $i -ge 3 ]; then echo "The size of the vector is <= 50."; fi
  if [ $i -ge 4 ]; then echo "How many maximum increaseing subsequences of this vector are there?"; fi
  if [ $i -ge 5 ]; then echo "Example 0: { 1, 3, 2, 6, 4, 5 } = Answer = 4"; fi
  if [ $i -ge 6 ]; then echo "Example 1: { 10, 9, 8, 7, 6, 5 } = Answer = 6"; fi
  if [ $i -ge 7 ]; then echo "Example 2: { 1, 2, 3, 4, 5, 6 } = Answer = 1"; fi
  if [ $i -ge 8 ]; then echo "Example 3: { 564,234,34,4365,424,2234,306,21,934,592,195,2395,2396,29345,13295423,23945,2} = Answer = 41"; fi
  read a
done

