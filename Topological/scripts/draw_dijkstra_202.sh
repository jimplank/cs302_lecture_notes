echo newgraph
echo xaxis min 0 max 11 nodraw size 7
echo yaxis min 0 max 19 nodraw size 5

i=0
while [ $i -lt 20 ]; do
  echo newline gray .8 pts 0 10 1 $i
  echo newline gray .8 pts 5 $i 6 10
  j=1
  while [ $j -lt 10 ]; do
    k=0
    while [ $k -lt 20 ]; do
      echo newline gray .8 pts $j $i $(($j+1)) $k
      k=$(($k+1))
    done
    j=$(($j+1))
  done
  i=$(($i+1))
done

echo newcurve marktype circle pts 0 10 11 10
i=0
while [ $i -lt 20 ]; do
  j=1
  while [ $j -le 10 ]; do
    echo $j $i
    j=$(($j+1))
  done
  i=$(($i+1))
done

sed '/Total/,$d' txt/dijkstra_200_10.txt |
  awk '{ if ($1 == 0) {
           fx = 0
           fy = 10
         } else {
           fx = ($1-1)/20 + 1
           fy = ($1-1)%20
         } 
         if ($2 == 201) {
           tx = 11
           ty = 10
         } else {
           tx = ($2-1)/20 + 1
           ty = ($2-1)%20
         } 
         printf("newline color 1 0 0 pts %d %d %d %d\n", fx, fy, tx, ty) }'
