echo newgraph
echo xaxis min 0 max 6 nodraw size 7
echo yaxis min 0 max 19 nodraw size 5

i=0
while [ $i -lt 20 ]; do
  echo newline gray .8 pts 0 10 1 $i
  echo newline gray .8 pts 5 $i 6 10
  j=1
  while [ $j -lt 5 ]; do
    k=0
    while [ $k -lt 20 ]; do
      echo newline gray .8 pts $j $i $(($j+1)) $k
      k=$(($k+1))
    done
    j=$(($j+1))
  done
  i=$(($i+1))
done

echo newcurve marktype circle pts 0 10 6 10
i=0
while [ $i -lt 20 ]; do
  echo 1 $i
  echo 2 $i
  echo 3 $i
  echo 4 $i
  echo 5 $i
  i=$(($i+1))
done

sed '/Total/,$d' txt/dijkstra_100_5.txt |
  awk '{ if ($1 == 0) {
           fx = 0
           fy = 10
         } else {
           fx = ($1-1)/20 + 1
           fy = ($1-1)%20
         } 
         if ($2 == 101) {
           tx = 6
           ty = 10
         } else {
           tx = ($2-1)/20 + 1
           ty = ($2-1)%20
         } 
         printf("newline color 1 0 0 pts %d %d %d %d\n", fx, fy, tx, ty) }'
