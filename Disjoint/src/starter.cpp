/* Skeleton for union by size.
 */

#include <vector>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include "disjoint.hpp"
using namespace std;

DisjointSetBySize::DisjointSetBySize(int nelements)
{
  /* Write this. */
}

int DisjointSetBySize::Find(int element)
{
  /* Write this */
}

int DisjointSetBySize::Union(int s1, int s2)
{
  /* Write this. */

  int p, c;
}

void DisjointSetBySize::Print() const
{
  size_t i;

  printf("\n");
  printf("Node:  ");
  for (i = 0; i < links.size(); i++) printf("%3lu", i);  
  printf("\n");

  printf("Links: ");
  for (i = 0; i < links.size(); i++) printf("%3d", links[i]);  
  printf("\n");

  printf("Sizes: ");
  for (i = 0; i < links.size(); i++) printf("%3d", sizes[i]);  
  printf("\n\n");
}

