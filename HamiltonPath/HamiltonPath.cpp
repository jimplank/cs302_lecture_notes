#include <string>
#include <vector>
#include <iostream>
#include <cstdio>
#include <cstdlib>
using namespace std;

class Vertex {
  public:
    int visited;
    int component;
    vector <int> adj;
};
class HamiltonPath {
  public:
    void ConComp(int n, int c);
    int Cycle_Det(int n, int from);
    vector <Vertex> V;
    int countPaths(vector <string> roads);
};

void HamiltonPath::ConComp(int n, int c)
{
  int i;

  if (V[n].component != -1) return;
  V[n].component = c;
  for (i = 0; i < V[n].adj.size(); i++) {
    ConComp(V[n].adj[i], c);
  }
}

int HamiltonPath::Cycle_Det(int n, int from)
{
  int i, j;

  if (V[n].visited != -1) return 1;

  V[n].visited = 1;
  for (i = 0; i < V[n].adj.size(); i++) {
    j = V[n].adj[i];
    if (j != from && Cycle_Det(j, n)) return 1;
  }
  return 0;
}

int HamiltonPath::countPaths(vector <string> roads)
{
  int i, j;
  int c, b;
  long long p;

  V.resize(roads.size());
  for (i = 0; i < V.size(); i++) {
    V[i].visited = -1;
    V[i].component = -1;
  }

  for (i = 0; i < roads.size(); i++) {
    for (j = 0; j < i; j++) {
      if (roads[i][j] == 'Y') {
        V[i].adj.push_back(j);
        V[j].adj.push_back(i);
      }
    }
  }
  for (i = 0; i < V.size(); i++) if (V[i].adj.size() > 2) return 0;
  for (i = 0; i < V.size(); i++) {
    if (V[i].visited == -1 && Cycle_Det(i, -1)) return 0;
  }
  c = 0;
  b = 0;
  for (i = 0; i < V.size(); i++) {
    if (V[i].component == -1) {
      if (V[i].adj.size() > 0) b++;
      ConComp(i, c);
      c++;
    }
  }

  p = 1;
  for (i = 2; i <= c; i++) {
    p *= i;
    p %= 1000000007;
  }
  for (i = 0; i < b; i++) {
    p *= 2;
    p %= 1000000007;
  }
  return p;
}
