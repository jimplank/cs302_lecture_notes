#include "HamiltonPath.cpp"

int main(int argc, char **argv)
{
  int i;
  class HamiltonPath TheClass;
  int retval;
  vector <string> roads;

  if (argc != 2) { fprintf(stderr, "usage: a.out num\n"); exit(1); }

/*
  roads = ;
*/


 if (atoi(argv[1]) == 0) {
    roads.push_back("NYN");
    roads.push_back( "YNN");
    roads.push_back( "NNN");
  }

 if (atoi(argv[1]) == 1) {
    roads.push_back("NYYY");
    roads.push_back( "YNNN");
    roads.push_back( "YNNN");
    roads.push_back( "YNNN");
  }

 if (atoi(argv[1]) == 2) {
    roads.push_back("NYY");
    roads.push_back( "YNY");
    roads.push_back( "YYN");
  }

 if (atoi(argv[1]) == 3) {
    roads.push_back("NNNNNY");
    roads.push_back( "NNNNYN");
    roads.push_back( "NNNNYN");
    roads.push_back( "NNNNNN");
    roads.push_back( "NYYNNN");
    roads.push_back( "YNNNNN");
  }

  retval = TheClass.countPaths(roads);
  cout << retval << endl;

  exit(0);
}
