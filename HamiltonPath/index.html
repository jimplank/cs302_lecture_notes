<UL>
<LI> <b>Topcoder 1000-Point problem from SRM 452, Division 2</b>
<LI> James S. Plank
<LI> Thu Oct 18 13:45:33 EDT 2012
<LI> <a href=http://community.topcoder.com/stat?c=problem_statement&pm=10572&rd=13906>Problem statement</a>.
</UL>

This is a wonderful Topcoder problem that draws from material that we've
learned about Depth-First-Search and enumeration.
<p>
The first thing to do is to wrap your head around the problem through the examples.
There are two graphs here, both with cities as nodes.  The first is a complete
graph, because there is a road from each city to each other city.  The second
is composed only of the edges representing roads that you have to take.  These
are specified by the adjacency matrix <b>roads</b>.
<p>
Let's go through the examples.  For each example, I'll have the adjacency matrix
on the left, and the graphs on the right.  The graph from <b>roads</b> will have
red edges.  The other edges in the complete graph will be in gray.

<p><center>
<table><tr><td valign=top width=340>
<p><center>
<table border=3>
<td>Example 0:<p><pre>{"NYN",
 "YNN",
 "NNN"}

Answer = 4
</pre>
</td>
<td><img src=Ex0.jpg width=100></td>
</table></center><p></td>

<td valign=top><p><center><table border=3>
<td>Example 1:<p><pre>{"NYYY",
 "YNNN",
 "YNNN",
 "YNNN"}

Answer = 0
</pre>
</td>
<td><img src=Ex1.jpg width=100></td>
</table></center><p></td></tr>

<tr><td valign=top><p><center><table border=3>
<td>Example 2:<p><pre>{"NYY",
 "YNY",
 "YYN"}
Answer = 0
</pre>
</td>
<td><img src=Ex2.jpg width=100></td>
</table></center><p></td>

<td valign=top><p><center><table border=3>
<td>Example 3:<p><pre>{"NNNNNY",
 "NNNNYN",
 "NNNNYN",
 "NNNNNN",
 "NYYNNN",
 "YNNNNN"}

Answer = 24
</pre>
</td>
<td><img src=Ex3.jpg width=189></td>
</table></center><p></td></tr></table></center><p>

Since you must visit all <i>N</i> cities and you must use exactly <i>N-1</i> roads,
you must take a simple path that visits all cities.  For that reason Examples 1
and 2 return zero, because there's no way to construct a simple path with those
graphs.  Put another way, when you consider the red edges, every node must have
two or fewer edges on its adjacency list, and there can be no cycles.   So, our
first programming task is going to be to create the graph with red edges, and
to return 0 when we discover cases like examples 1 and 2.
<p>
That code is in
<b><a href=Hamilton-1.cpp>Hamilton-1.cpp</a></b>.  The cycle detection
should look very familiar to you from the lecture notes on Depth-First-Search:

<p><center><table border=3 cellpadding=3><td><pre>
#include &lt;string&gt;
#include &lt;vector&gt;
#include &lt;iostream&gt;
#include &lt;cstdio&gt;
#include &lt;cstdlib&gt;
using namespace std;

class Vertex {
  public:
    int visited;
    vector &lt;int&gt; adj;
};

class HamiltonPath {
  public:
    int Cycle_Det(int n, int from);
    vector &lt;Vertex&gt; V;
    int countPaths(vector &lt;string&gt; roads);
};

int HamiltonPath::Cycle_Det(int n, int from)
{
  int i, j;

  if (V[n].visited != -1) return 1;

  V[n].visited = 1;
  for (i = 0; i &lt; V[n].adj.size(); i++) {
    j = V[n].adj[i];
    if (j != from && Cycle_Det(j, n)) return 1;
  }
  return 0;
}

int HamiltonPath::countPaths(vector &lt;string&gt; roads)
{
  int i, j;

  V.resize(roads.size());
  for (i = 0; i &lt; V.size(); i++) {
    V[i].visited = -1;
  }

  for (i = 0; i &lt; roads.size(); i++) {     // Create the graph
    for (j = 0; j &lt; roads.size(); j++) {
      if (roads[i][j] == 'Y') V[i].adj.push_back(j);
    }
  }

  /* Return 0 if a node has more than two edges coming from it. */
  for (i = 0; i &lt; V.size(); i++) if (V[i].adj.size() &gt; 2) return 0;

  /* Run Cycle Detection and return 0 if you discover a cycle */

  for (i = 0; i &lt; V.size(); i++) {
    if (V[i].visited == -1 && Cycle_Det(i, -1)) return 0;
  }

  /* Return 1 for now -- we'll finish this later. */
  return 1;
}
</pre></td></table></center><p>

To compile this, the file
<b><a href=Hamilton-Main.cpp>Hamilton-Main.cpp</a></b> includes <b>HamiltonPath.cpp</b>
and runs the examples from the command line.  So we copy <b>Hamilton-1.cpp</b>
to <b>HamiltonPath.cpp</b> and then compile <b>Hamilton-Main.cpp</b>.  Examples 1 and 
2 return 0 as expected, while the other two examples return 1:

<pre>
UNIX> <font color=darkred><b>cp Hamilton-1.cpp HamiltonPath.cpp</b></font>
UNIX> <font color=darkred><b>g++ Hamilton-Main.cpp</b></font>
UNIX> <font color=darkred><b>a.out 0</b></font>
1
UNIX> <font color=darkred><b>a.out 1</b></font>
0
UNIX> <font color=darkred><b>a.out 2</b></font>
0
UNIX> <font color=darkred><b>a.out 3</b></font>
1
UNIX> <font color=darkred><b></b></font>
</pre>

At this point, we have to figure out how to count the paths.  Let's consider
examples.  I'm only drawing the red edges now.  Here's a pretty simple example:

<p><center><table border=3><td><img src=Ex4.jpg width=132></td></table></center><p>

A path through the graph is equivalent to a permutation of the numbers 0, 1, 2, 3.
The number of paths is therefore (4!) = 24.  How about example 0 from the writeup:

<p><center><table border=3><td><img src=Ex5.jpg width=70></td></table></center><p>

Well, there are two connected components, and every path through the graph will
correspond to a permutation of the components.  However, for each permutation
of components, there are two ways to travel through nodes 0 and 1. Thus, the
number of paths is (2!)2 = 4.  Finally, let's look at example 3 from the writeup:

<p><center><table border=3><td><img src=Ex6.jpg width=117></td></table></center><p>

Again, each path corresponds to a permutation of the three components.  For the first
two components, there are two ways to go through the component.  Therefore, the 
number of paths is (3!)(2)(2) = 24.  
<p>
Do you see the pattern?  If there are <i>c</i> connected components and of these <i>c</i>,
there are <i>b</i> components that have two or more nodes, then the number of paths
through the graph is:
<p><center>
<i>(c!) 2<sup>b</sup></i>
</center><p>
So, to solve this problem, let's identify the connected components, which will give us
<i>c</i> and <i>b</i>.  Then we can calculate the above formula, taking the product 
modulo 1,000,000,007 at each step.  It's best to use a 64-bit integer for these calculations.
That's why you'll see the term <b>long long</b>.  The solution is in
<b><a href=Hamilton-2.cpp>Hamilton-2.cpp</a></b>

<p><center><table border=3 cellpadding=3><td><pre>
#include &lt;string&gt;
#include &lt;vector&gt;
#include &lt;iostream&gt;
#include &lt;cstdio&gt;
#include &lt;cstdlib&gt;
using namespace std;

class Vertex {
  public:
    int visited;
    int component;
    vector &lt;int&gt; adj;
};
class HamiltonPath {
  public:
    void ConComp(int n, int c);
    int Cycle_Det(int n, int from);
    vector &lt;Vertex&gt; V;
    int countPaths(vector &lt;string&gt; roads);
};

void HamiltonPath::ConComp(int n, int c)
{
  int i;

  if (V[n].component != -1) return;
  V[n].component = c;
  for (i = 0; i &lt; V[n].adj.size(); i++) {
    ConComp(V[n].adj[i], c);
  }
}

int HamiltonPath::Cycle_Det(int n, int from)
{
  int i, j;

  if (V[n].visited != -1) return 1;

  V[n].visited = 1;
  for (i = 0; i &lt; V[n].adj.size(); i++) {
    j = V[n].adj[i];
    if (j != from && Cycle_Det(j, n)) return 1;
  }
  return 0;
}

int HamiltonPath::countPaths(vector &lt;string&gt; roads)
{
  int i, j;
  int c, b;
  long long p;

  V.resize(roads.size());
  for (i = 0; i &lt; V.size(); i++) {
    V[i].visited = -1;
    V[i].component = -1;
  }

  for (i = 0; i &lt; roads.size(); i++) {
    for (j = 0; j &lt; i; j++) {
      if (roads[i][j] == 'Y') {
        V[i].adj.push_back(j);
        V[j].adj.push_back(i);
      }
    }
  }
  for (i = 0; i &lt; V.size(); i++) if (V[i].adj.size() &gt; 2) return 0;
  for (i = 0; i &lt; V.size(); i++) {
    if (V[i].visited == -1 && Cycle_Det(i, -1)) return 0;
  }

  /* Determine connected components */
  c = 0;
  b = 0;
  for (i = 0; i &lt; V.size(); i++) {
    if (V[i].component == -1) {
      if (V[i].adj.size() &gt; 0) b++;
      ConComp(i, c);
      c++;
    }
  }

  /* Calculate (c!)2^b */
  p = 1;
  for (i = 2; i &lt;= c; i++) {
    p *= i;
    p %= 1000000007;
  }
  for (i = 0; i &lt; b; i++) {
    p *= 2;
    p %= 1000000007;
  }
  return p;
}
</pre></td></table></center><p>

Compile and test:

<pre>
UNIX> <font color=darkred><b>cp Hamilton-2.cpp HamiltonPath.cpp</b></font>
UNIX> <font color=darkred><b>g++ Hamilton-Main.cpp </b></font>
UNIX> <font color=darkred><b>a.out 0</b></font>
4
UNIX> <font color=darkred><b>a.out 1</b></font>
0
UNIX> <font color=darkred><b>a.out 2</b></font>
0
UNIX> <font color=darkred><b>a.out 3</b></font>
24
UNIX> <font color=darkred><b></b></font>
</pre>

And submit!
