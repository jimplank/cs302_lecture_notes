#include <string>
#include <vector>
#include <iostream>
#include <cstdio>
#include <cstdlib>
using namespace std;

class Vertex {
  public:
    int visited;
    vector <int> adj;
};

class HamiltonPath {
  public:
    int Cycle_Det(int n, int from);
    vector <Vertex> V;
    int countPaths(vector <string> roads);
};

int HamiltonPath::Cycle_Det(int n, int from)
{
  int i, j;

  if (V[n].visited != -1) return 1;

  V[n].visited = 1;
  for (i = 0; i < V[n].adj.size(); i++) {
    j = V[n].adj[i];
    if (j != from && Cycle_Det(j, n)) return 1;
  }
  return 0;
}

int HamiltonPath::countPaths(vector <string> roads)
{
  int i, j;

  V.resize(roads.size());
  for (i = 0; i < V.size(); i++) {
    V[i].visited = -1;
  }

  for (i = 0; i < roads.size(); i++) {     // Create the graph
    for (j = 0; j < roads.size(); j++) {
      if (roads[i][j] == 'Y') V[i].adj.push_back(j);
    }
  }

  /* Return 0 if a node has more than two edges coming from it. */
  for (i = 0; i < V.size(); i++) if (V[i].adj.size() > 2) return 0;

  /* Run Cycle Detection and return 0 if you discover a cycle */

  for (i = 0; i < V.size(); i++) {
    if (V[i].visited == -1 && Cycle_Det(i, -1)) return 0;
  }

  /* Return 1 for now -- we'll finish this later. */
  return 1;
}
