<h3>Topcoder SRM 519, D2, 600-point problem - Solving with enumeration</h3>
<a href=http://web.eecs.utk.edu/~jplank>James S. Plank</a><br>
CS302<br>
September, 2016<br>
Latest revision: February, 2022.
<p>
<hr>
Problem description: <a href=http://community.topcoder.com/stat?c=problem_statement&pm=11554&rd=14544>http://community.topcoder.com/stat?c=problem_statement&pm=11554&rd=14544</a>.
<p>
<h3>In case Topcoder's servers are down</h3>

<UL>
<LI> You are flying through space, trying to get from a location labeled "Me" to a location
labeled "Home".  
<LI> Locations are defined by (x,y) coordinates, which are integers between 0 and 1,000,000,000.
<LI> You may only move horizontally or vertically, so, for example, the distance between
(3,5) and (10,10) is 12.
<LI> There are three teleports in your universe.  They are each defined by a string of the 
form:
<p><center><table border=3 cellpadding=3><td><pre>
"x1 y1 x2 y2"
</pre></td></table></center><p>
<LI> The teleports are special -- if you are at (x1,y1), then you can travel to (x2,y2) with 
a distance of 10.  The same is true for traveling from (x2,y2) to (x1,y1).
<LI> Your goal is to return the shortest distance from "Me" to "Home"
</UL>

The method signature for this problem is:

<p><center><table border=3 cellpadding=3><td><pre>
class ThreeTeleports {
  public:
    int shortestDistance(int xMe, int yMe,               <font color=blue>// "Me" is (xMe,yMe)</font>
                             int xHome, int yHome,       <font color=blue>// "Home" is (xHome,yHome)</font>
                             vector &lt;string&gt; teleports); <font color=blue>// Each teleport is "x1 y1 x2 y2"</font>
};
</pre></td></table></center><p>

<hr>
<h3>Examples</h3>

<pre>
#  Me             Home        Teleports                                                                              Answer
- ----- --------------------- -------------------------------------------------------------------------------------- ------
0  3  3          4          5 {"1000 1001 1000 1002", "1000 1003 1000 1004", "1000 1005 1000 1006"}                       3
1  0  0         20         20 {"1 1 18 20", "1000 1003 1000 1004", "1000 1005 1000 1006"}                                14
2  0  0         20         20 {"1000 1003 1000 1004", "18 20 1 1", "1000 1005 1000 1006"}                                14
3 10 10      10000      20000 {"1000 1003 1000 1004", "3 3 10004 20002", "1000 1005 1000 1006"}                          30
4  3  7      10000      30000 {"3 10 5200 4900", "12212 8699 9999 30011", "12200 8701 5203 4845"}                       117
5  0  0 1000000000 1000000000 {"0 1 0 999999999", "1 1000000000 999999999 0", "1000000000 1 1000000000 999999999"}       36
</pre>

Here's a picture of example 0.  The shortest path goes directly from "Me" to "Home".

<p><center><table border=3><td><img src=img/TT-EX0.jpg></td></table></center><p>

Here's a picture of example 1.  The shortest path is shaded in yellow.

<p><center><table border=3><td><img src=img/TT-EX1.jpg></td></table></center><p>

<hr>
<h3>Solving this problem with enumeration</h3>

Our driver code is in <b>Teleport-Main.cpp</b>, which includes
<b>ThreeTeleports.cpp</b>.  That is where we will implement the class and method.
(BTW, the topcoder writeup replaces the space-ship with a frog).
<p>
We're going to see this problem again later this semester.  However, its constraints are 
so small that we can solve it with enumeration.  There are only eight places
in the frog's universe that we care about:
<UL>
<LI> "Me" -- let's call that place 0.
<LI> "Home" -- let's call that place 7.
<LI> The two endpoints of the first teleport.  Let's call those places 1 and 2.
<LI> The two endpoints of the second teleport.  Let's call those places 3 and 4.
<LI> The two endpoints of the third teleport.  Let's call those places 5 and 6.
</UL>

The frog's path from "Me" to "Home" can be defined by an ordering of these places.
For example, 0-1-2-3-4-5-6-7 would be a path where the frog goes from:
<UL>
<LI> "Me" to the first endpoint of the first teleport, and then through the teleport.
<LI> From the second endpoint of the first teleport to the first endpoint of the second teleport, and then through the teleport.
<LI> From the second endpoint of the second teleport to the first endpoint of the third teleport, and then through the teleport.
<LI> From the second endpoint of the third teleport to "Home."
</UL>

0-2-1-3-4-5-6-7 would be the same path, but the frog would travel through the first 
teleport in reverse order.
<p>
We're going to solve this problem by enumerating the paths.  There are a few
details that we're going to have to take care of, though.
<p>
<hr>
<h3>Part 1: Reading those teleport strings</h3>

Our first task is to read those teleport strings.  What we're going to do is create
two vectors, <b>X</b> and <b>Y</b>, 
where <b>X[<i>i</i>]</b> is the <i>x</i> position of location <i>i</i>, and 
<b>Y[<i>i</i>]</b> is the <i>y</i> position of location <i>i</i>.  Our first 
program creates <b>X</b> and <b>Y</b> and prints them out.
It is in <b><a href=Teleport-1.cpp>Teleport-1.cpp</a></b>:

<p><center><table border=3 cellpadding=3><td><pre>
#include &lt;string&gt;
#include &lt;vector&gt;
#include &lt;algorithm&gt;
#include &lt;iostream&gt;
#include &lt;cstdio&gt;
#include &lt;cstdlib&gt;
using namespace std;

class ThreeTeleports {
  public:
    int shortestDistance(int xMe, int yMe, int xHome, int yHome, vector &lt;string&gt; teleports);
    vector &lt;int&gt; Order;
    void Permute(int index);
};

int ThreeTeleports::shortestDistance(int xMe, int yMe, int xHome, int yHome, 
                                     vector &lt;string&gt; teleports)
{
  vector &lt;long long&gt; X, Y;
  int i, x1, x2, y1, y2;

  <font color=blue>/* Create the vectors X and Y, which hold the x and y coordinates of the eight
     locations that we care about. */</font>

  X.push_back(xMe);
  Y.push_back(yMe);

  for (i = 0; i &lt; teleports.size(); i++) {
    sscanf(teleports[i].c_str(), "%d %d %d %d", &x1, &y1, &x2, &y2);
    X.push_back(x1);
    Y.push_back(y1);
    X.push_back(x2);
    Y.push_back(y2);
  }

  X.push_back(xHome);
  Y.push_back(yHome);
  
  <font color=blue>/* We use %lld to print out long long's using printf(). */</font>

  for (i = 0; i &lt; X.size(); i++) printf("[%lld,%lld]\n", X[i], Y[i]);

  return 0;
}
</pre></td></table></center><p>

I'm storing <b>X</b> and <b>Y</b> as <b>long long</b> vectors. The reason is that the 
constraints are pretty big: 1,000,000,000.  While that number fits easily into an
integer, three times that number does not, and since our paths may go from small
x/y values to high ones and back, we may have paths that are over 3,000,000,000.
So it's smart to use <b>long long</b>'s.  
<p>
We test it on examples 0 and 1, and everything looks good:

<pre>
UNIX> <font color=darkred><b>cp Teleport-1.cpp ThreeTeleports.cpp</b></font>
UNIX> <font color=darkred><b>g++ Teleport-Main.cpp</b></font>
UNIX> <font color=darkred><b>a.out 0</b></font>
[3,3]
[1000,1001]
[1000,1002]
[1000,1003]
[1000,1004]
[1000,1005]
[1000,1006]
[4,5]
0
UNIX> <font color=darkred><b>a.out 1</b></font>
[0,0]
[1,1]
[18,20]
[1000,1003]
[1000,1004]
[1000,1005]
[1000,1006]
[20,20]
0
UNIX>
</pre>

<hr>
<h3>Part 2: Creating an adjacency matrix from X and Y</h3>

Second, when we enumerate paths, we want to calculate the frog's distance 
along the paths.  To help us with this, we're going to create a data structure
called an <i>adjacency matrix.</i>  This is going to be a vector of vectors of
<b>long longs</b>.  Let's call it <b>AM</b>.  Then <b>AM[<i>i</i>][<i>j</i>]</b>
will hold the distance that the frog has to jump to go from place <i>i</i> to 
place <i>j</i>.

The code to create this 
is in
<b><a href=Teleport-2.cpp>Teleport-2.cpp</a></b>.  Here's the variable declaration
and the new code.  For the moment, I'm ignoring the teleports and just calculating
the frog's hopping distance between places:

<p><center><table border=3 cellpadding=3><td><pre>
  ...
  vector &lt; vector &lt;long long&gt; &gt;AM;
  ...
  <font color=blue>/* Create the adjacency matrix. */</font>

  AM.resize(X.size());
  for (i = 0; i &lt; X.size(); i++) {
    for (j = 0; j &lt; X.size(); j++) {
      xd = X[j]-X[i];
      if (xd &lt; 0) xd = -xd;
      yd = Y[j]-Y[i];
      if (yd &lt; 0) yd = -yd;
      AM[i].push_back(xd+yd);
    }
  }

  <font color=blue>/* Print the adjacency matrix. */</font>

  for (i = 0; i &lt; AM.size(); i++) {
    for (j = 0; j &lt; AM[i].size(); j++) {
      printf("%6lld", AM[i][j]);
    }
    printf("\n");
  }

  return 0;
}
</pre></td></table></center><p>

We run this on examples 0 and 1, verifying that it is correct:

<pre>
UNIX> <font color=darkred><b>cp Teleport-2.cpp ThreeTeleports.cpp</b></font>
UNIX> <font color=darkred><b>g++ Teleport-Main.cpp</b></font>
UNIX> <font color=darkred><b>a.out 0</b></font>
     0  1995  1996  1997  1998  1999  2000     3
  1995     0     1     2     3     4     5  1992
  1996     1     0     1     2     3     4  1993
  1997     2     1     0     1     2     3  1994
  1998     3     2     1     0     1     2  1995
  1999     4     3     2     1     0     1  1996
  2000     5     4     3     2     1     0  1997
     3  1992  1993  1994  1995  1996  1997     0
0
UNIX> <font color=darkred><b>a.out 1</b></font>
     0     2    38  2003  2004  2005  2006    40
     2     0    36  2001  2002  2003  2004    38
    38    36     0  1965  1966  1967  1968     2
  2003  2001  1965     0     1     2     3  1963
  2004  2002  1966     1     0     1     2  1964
  2005  2003  1967     2     1     0     1  1965
  2006  2004  1968     3     2     1     0  1966
    40    38     2  1963  1964  1965  1966     0
0
UNIX> <font color=darkred><b></b></font>
</pre>

<hr>
<h3>Part 3: Adding the teleports to the adjacency matrix</h3>

Next, we need to add the teleports to the adjacency matrix -- these are between 1/2, 3/4, 5/6,
and are equal to 10 jumps.  This is in
<b><a href=Teleport-3.cpp>Teleport-3.cpp</a></b>.  Here's the new code, which is added 
right before we print the adjacency matrix:

<p><center><table border=3 cellpadding=3><td><pre>
  ...
  <font color=blue>/* Add the teleports to the adjacency matrix. */</font>

  AM[1][2] = 10;
  AM[2][1] = 10;
  AM[3][4] = 10;
  AM[4][3] = 10;
  AM[5][6] = 10;
  AM[6][5] = 10;
  ...
</pre></td></table></center><p>

I've highlighed the teleports in the output below:

<pre>
UNIX> <font color=darkred><b>cp Teleport-3.cpp ThreeTeleports.cpp </b></font>
UNIX> <font color=darkred><b>g++ Teleport-Main.cpp</b></font>
UNIX> <font color=darkred><b>a.out 1</b></font>
     0     2    38  2003  2004  2005  2006    40
     2     0    <font color=blue><b>10</b></font>  2001  2002  2003  2004    38
    38    <font color=blue><b>10</b></font>     0  1965  1966  1967  1968     2
  2003  2001  1965     0    <font color=blue><b>10</b></font>     2     3  1963
  2004  2002  1966    <font color=blue><b>10</b></font>     0     1     2  1964
  2005  2003  1967     2     1     0    <font color=blue><b>10</b></font>  1965
  2006  2004  1968     3     2    <font color=blue><b>10</b></font>     0  1966
    40    38     2  1963  1964  1965  1966     0
0
UNIX> <font color=darkred><b></b></font>
</pre>

<hr>
<h3>Part 4: Generating all possible paths</h3>

Now we need to generate all possible paths.  We're going to be a little lazy doing this,
because we're in a "Topcoder" mindset: Let's solve the problem so that it's fast enough
for Topcoder, but let's not spend the extra work making it optimally fast.
<p>
Our first piece of laziness is the following: We're going to generate more paths than we need, by generating all permutations
of the numbers from 1 to 7.  We'll put zero in the front the permutation, and then treat that
as a path.  For example, the permutation 1,2,3,4,5,6,7 is the path 0-1-2-3-4-5-6-7, that
I explained above.
<p>
This approach generates more paths than we need for two reasons:

<UL>
<LI> With permutations like 1,2,7,3,4,5,6 our path will stop at 7, because the frog is "Home."
We'll generate many paths that start with 1,2,7, and they are all the same.  That's wasteful.
<LI> With permutations like 1,3,5,2,4,6,7, there's no way our frog would really take this
path, because there are no teleports.  The frog may as well just to straight from 0 to 7.
</UL>

We're going to ignore this wastefulness, though, because the number of permutations, (7!), is
5040, which is well fast enough for Topcoder.  We couldn't use this solution if there were
more teleports.

<p>
Our second piece of laziness is using <b>next_permutation()</b> from C++'s <b>algorithm</b>
library.  This generates the "next" lexicographic permutation of a vector, and we don't
need to do recursion.
<p>
The code in 
<b><a href=Teleport-4.cpp>Teleport-4.cpp</a></b> does this, and prints out the permutations.
What I do here is put all of the numbers from 0 through 7 into a vector <b>path</b>, and
keep calling <b>next_permutation()</b> until <b>path[0]</b> changes from 0 to 1. 
Here's the new code:

<p><center><table border=3 cellpadding=3><td><pre>
  ...
  <font color=blue>/* Generate and print the permutations of 1-7. 
     We do that with a vector containing the numbers 0 through 7,
     and keep calling next_permutation() until path[0] changes from 0 to 1. */</font>
   
  for (i = 0; i &lt; 8; i++) path.push_back(i);
  while (path[0] == 0) {
    for (i = 0; i &lt; path.size(); i++) printf(" %d", path[i]);
    printf("\n");
    next_permutation(path.begin(), path.end());
  }
  
  return 0;
}
</pre></td></table></center><p>

<pre>
UNIX> <font color=darkred><b>cp Teleport-4.cpp ThreeTeleports.cpp</b></font>
UNIX> <font color=darkred><b>g++ Teleport-Main.cpp</b></font>
UNIX> <font color=darkred><b>a.out 0 | head -n 10</b></font>
 0 1 2 3 4 5 6 7
 0 1 2 3 4 5 7 6
 0 1 2 3 4 6 5 7
 0 1 2 3 4 6 7 5
 0 1 2 3 4 7 5 6
 0 1 2 3 4 7 6 5
 0 1 2 3 5 4 6 7
 0 1 2 3 5 4 7 6
 0 1 2 3 5 6 4 7
 0 1 2 3 5 6 7 4
UNIX> <font color=darkred><b>a.out 0 | tail -n 10</b></font>
 0 7 6 5 3 2 4 1
 0 7 6 5 3 4 1 2
 0 7 6 5 3 4 2 1
 0 7 6 5 4 1 2 3
 0 7 6 5 4 1 3 2
 0 7 6 5 4 2 1 3
 0 7 6 5 4 2 3 1
 0 7 6 5 4 3 1 2
 0 7 6 5 4 3 2 1
0
UNIX> <font color=darkred><b>a.out 0 | wc</b></font>
    5041   40321   85682
UNIX> <font color=darkred><b></b></font>
</pre>

There are 5041 lines, because the last line prints the return value of 
the procedure.

<hr>
<h3>Part 5: Calculating the path lengths and returning the minimum. </h3>

The final piece of code takes each permutation and calculates a path length 
from it, keeping track of the minimum.  I start by setting my minimum to a
number larger than it can possibly be -- in this case, I choose a gigantic
<b>long long</b> -- this is a one followed by 36 zeros, or 2<sup>36</sup>.
I specify the number in hexadecimal (remember, each hex digit is 4 bits, so 
I have 9 zeros to make 2<sup>36</sup>.).
I have to put "LL" at the end of the to tell the compiler that it's a <b>long long</b>.
<p>
Here's the new code (in 
<b><a href=Teleport-5.cpp>Teleport-5.cpp</a></b>):

<p><center><table border=3 cellpadding=3><td><pre>
  ...
  minimum = 0x1000000000LL;   <font color=blue>/* This number is 2^36. */</font>

  for (i = 0; i &lt; 8; i++) path.push_back(i);
  while (path[0] == 0) {
 
    <font color=blue>/* Here is where we calculate the path length. */</font>     
    d = 0;                    
    for (i = 0; path[i] != 7; i++) {
      d += AM[path[i]][path[i+1]];
    }
    if (d &lt; minimum) minimum = d;
    next_permutation(path.begin(), path.end());
  }
  
  return minimum;
}
</pre></td></table></center><p>

This solves the problem fast enough for topcoder, so if all we cared about was solving the
topcoder problem, we'd be done.  
<p>
But.....

<p>
<hr>
<h3>Doing the proper enumeration</h3>

As a mention in part 4 above, the enumeration is wasteful, because it generates too many
paths that are either redundant, or are paths that the frog would never take.  I'm going
to do this correctly here, just because I wouldn't be a good teacher if I didn't.
<p>
What I'm going to do is enumerate teleport orderings.  I'm going to label them by their lowest
indices in <b>path</b>: 1, 3 and 5, and I need to enumerate all possible orderings of zero
through three teleports.  Let's start with that code first. I'm going to write a recursive
proecedure <b>enumerate_teleports</b>, which will enumerate all strings that contain at most
one "1", one "3" and one "5".  This is pretty much identical to the recursive permutation
enumeration from the Enumeration lecture notes, except at the beginning of the recursive
procedure, we store the first <b>index</b> characters of the working string into the 
vector <b>all_orders</b>.  At the end, <b>all_orders</b> will contain the strings we want.
<p>
The code is in <b><a href=Teleport-6.cpp>Teleport-6.cpp</a></b>.  Here's the recursive 
procedure:

<p><center><table border=3 cellpadding=3><td><pre>
<font color=blue>/* Enumerate all orderings of teleports.  We'll do this by having "work" start as "135",
   and then we recursively enumerate permutations, storing each prefix when we enter the
   recursive procedure. */</font>

void enumerate_teleports(int index, string &work, vector &lt;string&gt; &all_orders)
{
  char tmp;
  size_t i;

  all_orders.push_back(work.substr(0, index));  <font color=blue>// This stores the prefix - first index characters</font>
  for (i = index; i &lt; work.size(); i++) {
    tmp = work[i];
    work[i] = work[index];
    work[index] = tmp;
    enumerate_teleports(index+1, work, all_orders);
    tmp = work[i];
    work[i] = work[index];
    work[index] = tmp;
  }
}
</pre></td></table></center><p>

And here's the code that prints out the orderings:

<p><center><table border=3 cellpadding=3><td><pre>
  <font color=blue>/* Do the enumeration of teleport orderings and print it out. */</font>

  work = "135";
  enumerate_teleports(0, work, all_orders);
  for (i = 0; i &lt; all_orders.size(); i++) cout &lt;&lt; all_orders[i] &lt;&lt; endl;
  exit(0);
</pre></td></table></center><p>

We run it, and you'll see it prints all of the teleport orderings (including the empty string):

<pre>
UNIX> <font color=darkred><b>cp Teleport-6.cpp ThreeTeleports.cpp </b></font>
UNIX> <font color=darkred><b>g++ Teleport-Main.cpp </b></font>
UNIX> <font color=darkred><b>./a.out 0</b></font>

1
13
135
15
153
3
31
315
35
351
5
53
531
51
513
UNIX> <font color=darkred><b></b></font>
</pre>

<hr>
<h3>Doing a power-set enumeration of how we travel through the teleports</h3>

Suppose we have an ordering of teleports, like "13".  We need to now enumerate
all of the ways that we can travel through each teleport.  With "13", there will
be four such ways -- I'll show them as paths:

<pre>
1:     0 -> 1 -> 2 -> 3 -> 4 -> 7
2:     0 -> 2 -> 1 -> 3 -> 4 -> 7
3:     0 -> 1 -> 2 -> 4 -> 3 -> 7
4:     0 -> 2 -> 1 -> 4 -> 3 -> 7
</pre>

We can do this enumeration with a power set enumeration -- if there are <i>n</i>
teleports, we enumerate all possible strings of length <i>n</i> composed of
the characters '<' and '>'. Each teleport corresponds to a character in the string.
If the character is '<', then we travel the teleport in one direction, and if it
is '>', then we travel it in the other direction.

The code to generate these strings is in
<b><a href=Teleport-7.cpp>Teleport-7.cpp</a></b>:

<p><center><table border=3 cellpadding=3><td><pre>
  <font color=blue>/* For each ordering of teleports (t), do a power set enumeration to determine
     the order through the teleports (o).  Print them out. */</font>

  for (i = 0; i &lt; all_orders.size(); i++) {
    t = all_orders[i];
    for (j = 0; j &lt; (1 &lt;&lt; t.size()); j++) {
      o = "";
      for (k = 0; k &lt; t.size(); k++) {
        if (j & (1 &lt;&lt; k)) o.push_back('&lt;'); else o.push_back('&gt;');
      }
      printf("%3s %3s\n", t.c_str(), o.c_str());
    }
  }
}
</pre></td></table></center><p>

Since there are a lot of strings to print (79), I just show the first 15, but you can see
how the power set enumeration works:

<pre>
UNIX> <font color=darkred><b>cp Teleport-7.cpp ThreeTeleports.cpp </b></font>
UNIX> <font color=darkred><b>g++ Teleport-Main.cpp </b></font>
UNIX> <font color=darkred><b>a.out 0 | head -n 15</b></font>
       
  1   >
  1   <
 13  >>
 13  <>
 13  ><
 13  <<
135 >>>
135 <>>
135 ><>
135 <<>
135 >><
135 <><
135 ><<
135 <<<
UNIX>
</pre>

<hr>
<h3> Finishing up -- calculating the path lengths</h3>

Finally, I write a procedure to calculate the path length from the two strings
and the adjacency matrix, and then calculate the minimum length.  Here's the 
procedure (in <b><a href=Teleport-8.cpp>Teleport-8.cpp</a></b>).

<p><center><table border=3 cellpadding=3><td><pre>
<font color=blue>/* Calculate the path lengths from the two strings: 

     t is the ordering of the teleports, composed of the characters '1', '3' and '5'
     o is the ordering through the teleports, composed of the characters '&lt;' and '&gt;'
     AM is the adjacency matrix
 */</font>

long long path_length(const string &t, 
                      const string &o, 
                      const vector &lt; vector &lt;long long &gt; &gt; &AM)
{
  vector &lt;int&gt; path;
  int i;
  long long length;

  <font color=blue>/* Create the path vector */</font>

  path.push_back(0);
  for (i = 0; i &lt; t.size(); i++) {
    if (o[i] == '&lt;') {
      path.push_back(t[i]-'0');
      path.push_back(t[i]-'0'+1);
    } else {
      path.push_back(t[i]-'0'+1);
      path.push_back(t[i]-'0');
    }
  }
  path.push_back(7);

  <font color=blue>/* Use that to calculate the path length */</font>

  length = 0;
  for (i = 1; i &lt; path.size(); i++) {
    length += AM[path[i-1]][path[i]];
  }
  return length;
}
</pre></td></table></center><p>

And here's the code where we do the enumeration and calculate the minimum:

<p><center><table border=3 cellpadding=3><td><pre>
  <font color=blue>/* For each ordering of teleports (t), do a power set enumeration to determine
     the order through the teleports (o).  Use path_lengh() to calculate the path
     lengths and store the minimum. */</font>

  minimum = path_length("", "", AM);   <font color=blue>/* Handle this case, because it isn't handled in
                                          the enumeration below. */</font>

  for (i = 0; i &lt; all_orders.size(); i++) {
    t = all_orders[i];
    for (j = 0; j &lt; (1 &lt;&lt; t.size()); j++) {
      o = "";
      for (k = 0; k &lt; t.size(); k++) {
        if (j & (1 &lt;&lt; k)) o.push_back('&lt;'); else o.push_back('&gt;');
      }
      d = path_length(t, o, AM);
      if (d &lt; minimum) minimum = d;
    }
  }
  return minimum;
}
</pre></td></table></center><p>

We're done!  And instead of enumerating 5040 times, we're only calling <b>path_length()</b> 79 times!

<pre>
UNIX> <font color=darkred><b>cp Teleport-8.cpp ThreeTeleports.cpp ; g++ Teleport-Main.cpp </b></font>
UNIX> <font color=darkred><b>for i in 0 1 2 3 4 5 ; do ./a.out $i ; done</b></font>
3
14
14
30
117
36
UNIX> <font color=darkred><b></b></font>
</pre>
