/* Do a power-set enumeration for the directions through the teleports.  Print them out. */

#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
#include <cstdio>
#include <cstdlib>
using namespace std;

class ThreeTeleports {
  public:
    int shortestDistance(int xMe, int yMe, int xHome, int yHome, vector <string> teleports);
};

/* Enumerate all orderings of teleports.  We'll do this by having "work" start as "135",
   and then we recursively enumerate permutations, storing each prefix when we enter the
   recursive procedure. */

void enumerate_teleports(int index, string &work, vector <string> &all_orders)
{
  char tmp;
  size_t i;

  all_orders.push_back(work.substr(0, index));  // This stores the prefix - first index characters
  for (i = index; i < work.size(); i++) {
    tmp = work[i];
    work[i] = work[index];
    work[index] = tmp;
    enumerate_teleports(index+1, work, all_orders);
    tmp = work[i];
    work[i] = work[index];
    work[index] = tmp;
  }
}

int ThreeTeleports::shortestDistance(int xMe, int yMe, int xHome, int yHome, 
                                     vector <string> teleports)
{
  vector <long long> X, Y;
  vector < vector <long long> >AM;
  int i, j, x1, x2, y1, y2, k;
  long long xd, yd;
  vector <string> all_orders;
  string work, t, o;

  /* Create the vectors X and Y, which hold the x and y coordinates of the eight
     locations that we care about. */

  X.push_back(xMe);
  Y.push_back(yMe);

  for (i = 0; i < teleports.size(); i++) {
    sscanf(teleports[i].c_str(), "%d %d %d %d", &x1, &y1, &x2, &y2);
    X.push_back(x1);
    Y.push_back(y1);
    X.push_back(x2);
    Y.push_back(y2);
  }

  X.push_back(xHome);
  Y.push_back(yHome);
  
  /* Create the adjacency matrix. */

  AM.resize(X.size());
  for (i = 0; i < X.size(); i++) {
    for (j = 0; j < X.size(); j++) {
      xd = X[j]-X[i];
      if (xd < 0) xd = -xd;
      yd = Y[j]-Y[i];
      if (yd < 0) yd = -yd;
      AM[i].push_back(xd+yd);
    }
  }

  /* Add the teleports to the adjacency matrix. */

  AM[1][2] = 10;
  AM[2][1] = 10;
  AM[3][4] = 10;
  AM[4][3] = 10;
  AM[5][6] = 10;
  AM[6][5] = 10;

  /* Do the enumeration of teleport orderings and print it out. */

  work = "135";
  enumerate_teleports(0, work, all_orders);

  /* For each ordering of teleports (t), do a power set enumeration to determine
     the order through the teleports (o).  Print them out. */

  for (i = 0; i < all_orders.size(); i++) {
    t = all_orders[i];
    for (j = 0; j < (1 << t.size()); j++) {
      o = "";
      for (k = 0; k < t.size(); k++) {
        if (j & (1 << k)) o.push_back('<'); else o.push_back('>');
      }
      printf("%3s %3s\n", t.c_str(), o.c_str());
    }
  }
  exit(0);

  return 0;
}
