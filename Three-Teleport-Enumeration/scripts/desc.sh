for i in 0 1 2 3 4 5 6 ; do
  clear
  if [ $i -ge 0 ]; then echo 'You are flying through space, trying to get from a location labeled "Me" to a location labeled "Home".' ; fi
  if [ $i -ge 1 ]; then echo 'Locations are defined by (x,y) coordinates, which are integers between 0 and 1,000,000,000.' ; fi
  if [ $i -ge 2 ]; then echo 'You may only move horizontally or vertically, so, for example, the distance between (3,5) and (10,10) is 12.' ; fi
  if [ $i -ge 3 ]; then echo 'There are three teleports in your universe. They are each defined by a string of the form: "x1 y1 x2 y2".' ; fi
  if [ $i -ge 4 ]; then echo 'The teleports are special -- if you are at (x1,y1), then you can travel to (x2,y2) with a distance of 10.' ; fi
  if [ $i -ge 5 ]; then echo 'The teleports are bidirectional.' ; fi
  if [ $i -ge 6 ]; then echo 'Your goal is to return the shortest distance from "Me" to "Home".' ; fi
  read a
done

