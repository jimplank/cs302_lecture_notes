bool Sudoku::Is_Row_Ok(int r) const
{
  char i;

  for (i = '1'; i <= '9'; i++) {
    if (puzzle[i].find(i) != puzzle[i].find_last_of(i)) return false;
  }
  return true;
}
