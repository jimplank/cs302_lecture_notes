if [ $# -ne 2 ]; then
  echo 'usage: sh scripts/best_of_last.sh #tournaments min-played' >&2
  exit 1
fi

st=`ls standings | tail -n $1 | sed 's/^/standings\//'`
sa=`ls salaries | tail -n $1 | sed 's/^/salaries\//'`

cat $st $sa | bin/values | awk '{ if ($2 >= '$2') print $0 }' | sort -nr
