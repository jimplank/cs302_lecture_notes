{
  "golfers": {
    "Aaron Baddeley": 75,
    "Adam Hadwin": 93,
    "Adam Long": 74.5,
    "Adam Schenk": 65.5,
    "Andrew Landry": 27,
    "Andrew Putnam": 24.5,
    "Angus Flanagan": 14,
    "Austin Cook": 16,
    "Austin Eckroat": 84.5,
    "Beau Hossler": 71.5,
    "Ben Crane": 21.5,
    "Ben Martin": 32.5,
    "Ben Taylor": 40,
    "Bill Haas": 23,
    "Bo Hoag": 79.5,
    "Bo Van Pelt": 76,
    "Brandon Hagy": 20.5,
    "Brandon Stone": 35.5,
    "Brandt Snedeker": 86,
    "Brendon Todd": 28,
    "Brent Snyder": 19.5,
    "Brian Gay": 40,
    "Brian Stuard": 89.5,
    "Brice Garnett": 78,
    "Bronson Burgoon": 32.5,
    "Bubba Watson": 58,
    "Byeong Hun An": 26.5,
    "Cameron Champ": 124,
    "Cameron Davis": 83,
    "Cameron Percy": 67,
    "Cameron Tringale": 86.5,
    "Camilo Villegas": 70,
    "Charl Schwartzel": 106,
    "Charles Howell III": 68,
    "Chase Seiffert": 69,
    "Chesson Hadley": 65.5,
    "Chez Reavie": 83,
    "Chris Baker": 59.5,
    "Chris Kirk": 28.5,
    "D.A. Points": 31.5,
    "D.J. Trahan": 31.5,
    "David Hearn": 71.5,
    "David Lingmerth": 72,
    "Denny McCarthy": 62.5,
    "Dominic Bozzelli": 27,
    "Doug Ghim": 34.5,
    "Dustin Johnson": 28.5,
    "Dylan Frittelli": 21.5,
    "Emiliano Grillo": 29,
    "Erik Van Rooyen": 69,
    "Fabian Gomez": 29.5,
    "Gary Woodland": 90.5,
    "Grayson Murray": 14.5,
    "Greg Chalmers": 27.5,
    "Hank Lebioda": 39.5,
    "Harry Higgs": 28,
    "Hunter Mahan": 27,
    "J.J. Spaun": 31.5,
    "J.T. Poston": 81,
    "James Hahn": 32.5,
    "Jason Dufner": 77,
    "Jhonattan Vegas": 108.5,
    "Jimmy Walker": 77.5,
    "Joel Dahmen": 61.5,
    "John Huh": 23,
    "John Pak": 24,
    "John Senden": 21.5,
    "Johnson Wagner": 29.5,
    "Jonathan Byrd": 66.5,
    "Joseph Bramlett": 62.5,
    "Josh Teater": 65,
    "Justin Quiban": 22,
    "Keegan Bradley": 67,
    "Keith Mitchell": 104,
    "Kevin Stadler": 27,
    "Kevin Tway": 35.5,
    "Kiradech Aphibarnrat": 68.5,
    "Kris Ventura": 20.5,
    "Kyle Stanley": 29,
    "Kyoung-Hoon Lee": 98.5,
    "Lanto Griffin": 27,
    "Louis Oosthuizen": 106,
    "Lucas Herbert": 28,
    "Luke Donald": 74,
    "Luke List": 71,
    "MJ Daffue": 65,
    "Mark Anderson": 23,
    "Mark Hubbard": 86,
    "Martin Trainer": 63,
    "Matt Every": 25.5,
    "Matt Kuchar": 34.5,
    "Matthew NeSmith": 33.5,
    "Matthew Wolff": 74.5,
    "Maverick McNealy": 79.5,
    "Michael Gellerman": 79,
    "Michael Gligic": 64.5,
    "Michael Kim": 76,
    "Michael Thompson": 68,
    "Mito Pereira": 88,
    "Nate Lashley": 32,
    "Nelson Ledesma": 15,
    "Nick Watney": 67,
    "Norman Xiong": 31,
    "Pat Perez": 83.5,
    "Patrick Reed": 76.5,
    "Patrick Rodgers": 67.5,
    "Patton Kizzire": 79,
    "Peter Malnati": 31,
    "Quade Cummins": 23.5,
    "Rafa Cabrera Bello": 67.5,
    "Rafael Campos": 21.5,
    "Rhein Gibson": 23,
    "Richy Werenski": 15.5,
    "Rickie Fowler": 78.5,
    "Ricky Barnes": 20,
    "Rob Oppenheim": 34,
    "Robby Shelton": 28,
    "Robert Macintyre": 26.5,
    "Robert Streb": 33.5,
    "Roger Sloan": 78,
    "Russell Knox": 24.5,
    "Ryan Armour": 87.5,
    "Ryan Blaum": 38,
    "Ryan Brehm": 70,
    "Ryan Hall": 25,
    "Ryan Moore": 35,
    "Sam Ryder": 83,
    "Sang-Moon Bae": 2.5,
    "Satoshi Kodaira": 20,
    "Scott Brown": 61,
    "Scott Harrington": 27.5,
    "Scott Piercy": 58.5,
    "Scott Stallings": 76,
    "Sean O'Hair": 30,
    "Sebastian Cappelen": 20.5,
    "Sepp Straka": 34,
    "Sergio Garcia": 79.5,
    "Shawn Stefani": 29.5,
    "Stephen Stallings Jr.": 30,
    "Steve Stricker": 20.5,
    "Stewart Cink": 27,
    "Sung Kang": 67.5,
    "Ted Potter Jr.": 28.5,
    "Tom Hoge": 23,
    "Tom Lewis": 62.5,
    "Tom Lovelady": 19.5,
    "Tony Finau": 74,
    "Troy Merritt": 73,
    "Tyler Duncan": 33,
    "Tyler McCumber": 32,
    "Vaughn Taylor": 14.5,
    "Vincent Whaley": 27.5,
    "Wes Roach": 22.5,
    "Will Gordon": 24.5,
    "Wyndham Clark": 34,
    "Zack Sucher": 24
  },
  "tournament": "2021-07-25-3M-Open",
  "value": "score"
}
