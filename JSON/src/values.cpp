#include "nlohmann/json.hpp"
#include <iostream>
#include <vector>
#include <sstream>
using namespace std;
using nlohmann::json;

int main()
{
  json js;
  json overall;
  string v, t, n;
  double s, sal, total;
  size_t i;
  json g;
  json salaries, scores;
  json::const_iterator jit1, jit2;

  overall["salary"] = json::object();
  overall["score"] = json::object();

  try {
    while (true) {
      cin >> js;
      v = js["value"];
      t = js["tournament"];
      g = js["golfers"];
      overall[v][t] = g;
    }
  } catch (const json::exception &e) {
  }

  salaries = overall["salary"];
  scores = overall["score"];
  g = json::object();

//  cout << overall << endl;

  for (jit1 = scores.begin(); jit1 != scores.end(); jit1++) {
    t = jit1.key();
    js = jit1.value();
    for (jit2 = js.begin(); jit2 != js.end(); jit2++) {
      n = jit2.key();
      s = jit2.value();
      if (salaries[t].contains(n)) {
        sal = salaries[t][n];
        g[n].push_back(s/sal*1000.0);
      }
    }
  } 

  for (jit1 = g.begin(); jit1 != g.end(); jit1++) {
    total = 0;
    for (i = 0; i < jit1.value().size(); i++) {
      total += jit1.value().at(i).get<double>();
    }
    total /= (double) jit1.value().size();
    printf("%7.4lf %2d %s\n", total, (int) jit1.value().size(), jit1.key().c_str());
  }

  return 0;
}
