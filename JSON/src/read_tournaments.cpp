#include "nlohmann/json.hpp"
#include <iostream>
#include <vector>
#include <sstream>
using namespace std;
using nlohmann::json;

int main()
{
  json js;
  json overall;
  string v, t;
  json g;

  overall["salary"] = json::object();
  overall["score"] = json::object();

  try {
    while (true) {
      cin >> js;
      v = js["value"];
      t = js["tournament"];
      g = js["golfers"];
      overall[v][t] = g;
    }
  } catch (const json::exception &e) {
  }

  cout << overall << endl;
  return 0;
}
